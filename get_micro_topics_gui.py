## This code written in 2020 by Maha BOUZAIENE <bouzaiene.maha@gmail.com> is licensed under CC BY-NC-SA
## This license allows reusers to distribute, remix, adapt, and build upon the material in any medium
## or format for noncommercial purposes only, and only so long as attribution is given to the creator.
## If you remix, adapt, or build upon the material, you must license the modified material under
## identical terms. 
## CC BY-NC-SA includes the following elements:
## BY – Credit must be given to the creator
## NC – Only noncommercial uses of the work are permitted
## SA – Adaptations must be shared under the same terms
## For further information see https://creativecommons.org/about/cclicenses/
## or look into the directory: ./copyright_NOTICES

import sys
import io
import glob
from pprint  import pprint
import pandas as pd
import json
import pickle
import os
from pathlib import Path
from configparser import ConfigParser
import tensorflow as tf
from split_document import Extraction
from stanza_to_list_of_indexed_objects import Stanza_Output

from bert_to_list_of_indexed_objects import BERT_Output

from dynprogalign import dynprogalign, alignment_info
from collectFiles import getListOfFiles
from merge_annotations import *
from text_span_v2 import *
from unification import *
from unification.match import *
from split_document import Extraction

def get_micro_topics():
  try:
    conf_parser = ConfigParser( allow_no_value = True )
    conf_file = Path( __file__ ).parent / "config.ini"
    conf_parser.read( conf_file )
    extract_abstract = conf_parser.getboolean( 'configuration', 'extract_abstract' )
    print( 'config use abstract =={0}'.format( extract_abstract ))
    the_file_lst_path = None
    if conf_parser.get( 'configuration', 'input_file_lst' ) != '':
      the_file_lst_path = conf_parser.get( 'configuration', 'input_file_lst' )
    file_lst = []
    if the_file_lst_path is not None:
      try:
        with open( the_file_lst_path , 'r', encoding = 'utf8' ) as idesc:
           file_lst = idesc.read().strip().split()
      except IOError as e:
        print( 'ERREUR à l\'ouverture de {0}'.format( the_file_lst_path ))
        return e.errno
    else:
      try:
        file_lst = [ x.replace( conf_parser.get( 'configuration', 'corpus_dir' ), '').replace( conf_parser.get( 'configuration', 'txt_sufx'), '')
                     for x in glob.glob( conf_parser.get( 'configuration', 'corpus_dir' ) +  '*' + conf_parser.get( 'configuration', 'txt_sufx' ),
                                         recursive = False) ]
      except IOError as e:
        print( 'ERREUR à l\'ouverture de {0}'.format( the_file_lst_nm ))
        return e.errno
    assert( conf_parser.get( 'configuration', 'corpus_dir' ) != conf_parser.get( 'configuration', 'abstract_dir' ) )
    assert( file_lst != [] )
    
  #----------------------------Get Abstract-------------------------------------------------------
    if extract_abstract == True:
      corpus_dir = conf_parser.get( 'configuration', 'corpus_dir' )
      txt_sufx = conf_parser.get( 'configuration', 'txt_sufx' )
      body_dir = conf_parser.get( 'configuration', 'body_dir' ) # not used for now
      abstract_dir = conf_parser.get( 'configuration', 'abstract_dir' )
      file_to_remove = []
      for file_name in file_lst:
        getAbstract = Extraction( corpus_dir + file_name + txt_sufx, body_dir, abstract_dir)
        getAbstract.abstractextraction()
        if ( Path(abstract_dir + file_name + txt_sufx).is_file() ):
            if ( os.stat( abstract_dir + file_name + txt_sufx ).st_size == 0 ):
                print( 'WARNING: removing empty abstract for file {0}'.format( file_name ) )
                os.remove(abstract_dir + file_name + txt_sufx)
                file_to_remove.append( file_name )
      if len(file_to_remove)!=0:
        for fnm in file_to_remove:
          file_lst.remove(fnm)
      input_dir = abstract_dir
    else:
       input_dir = conf_parser.get( 'configuration', 'corpus_dir' )


  #-----------------------------------------------------------------------------------------------
    stanza_func = Stanza_Output( lang = 'en',
                                 srcdir = input_dir,
                                 file_lst = file_lst,
                                 sufx = conf_parser.get( 'configuration', 'txt_sufx' ) )
    last_file_idx = stanza_func.curr_file_idx

  #-----------------------------------------------------------------------------------------------    
    bert_func = BERT_Output( lang = 'en',
                                 srcdir = input_dir,
                                 file_lst = file_lst,
                                 sufx = conf_parser.get( 'configuration', 'txt_sufx' ) )

    stanza_res = dict()
    bert_res = dict()
    res_realignement = dict()
    ##Indexed Object to MWU
    res_indexed_object_to_mwu = dict()
    pointers_mwu = dict()
    sentence_tokens_pointers = dict()
    triplets = dict()
    for fnm_idx in range(len(file_lst)):
  #----------------------------stanza processing of the whole corpus        
      last_tok_stanza = None
      tmp_list_stanza = []
      tok_stanza = stanza_func.next_tok()
      while tok_stanza:
        if ( (tok_stanza.annotations['text'].lower()=='which') and (last_tok_stanza!=None) ):
          tok_stanza.myrepr = last_tok_stanza.__repr__()
          tok_stanza.annotations['text'] = last_tok_stanza.annotations['text']
          tok_stanza.annotations['w_lemma'] = last_tok_stanza.annotations['w_lemma']
          tok_stanza.annotations['w_text'] = last_tok_stanza.annotations['w_text']
          tok_stanza.annotations['end_char'] = tok_stanza.annotations['start_char']+ last_tok_stanza.annotations['end_char']-last_tok_stanza.annotations['start_char']
          tok_stanza.annotations['w_upos'] = last_tok_stanza.annotations['w_upos']
          tok_stanza.annotations['w_xpos'] = last_tok_stanza.annotations['w_xpos']
          
        tmp_list_stanza.append( tok_stanza )

      
        if  ( tok_stanza.annotations['text'] != ',' ):
          last_tok_stanza = tok_stanza
        else:
          last_tok_stanza =  last_tok_stanza

        tok_stanza = stanza_func.next_tok()
        if not tok_stanza:
          print( 'stanza processed file {0}'.format( stanza_func.file_lst[ fnm_idx ] ))
          stanza_res[stanza_func.file_lst[ fnm_idx ]] = tmp_list_stanza
          if fnm_idx == len(file_lst)-1:
            break

  #----------------------------BERT processing of the whole corpus
      last_tok_bert = None    
      tmp_list_bert = []
      tok_bert = bert_func.next_tok()
      while tok_bert:
        if ( ((tok_bert.__repr__()).lower()=='which') and (last_tok_bert != None) ):
          tok_bert.myrepr = last_tok_bert.__repr__()
          tok_bert.annotations = last_tok_bert.annotations
      
        tmp_list_bert.append( tok_bert )


        if( tok_bert.__repr__() != ',' ):
          last_tok_bert = tok_bert
        else:
          last_tok_bert = last_tok_bert
        
        tok_bert = bert_func.next_tok()
        if not tok_bert:
          print( 'bert processed file {0}'.format( bert_func.file_lst[ bert_func.curr_file_idx ] ))
          bert_res[bert_func.file_lst[ bert_func.curr_file_idx ]] = tmp_list_bert
          #tmp_list_bert = []
          if fnm_idx == len(file_lst)-1:
            break

# Realignement
      print("BERT Done!")    
      #print(tmp_list_bert[0].annotations)
      aligner = dynprogalign( tmp_list_stanza , tmp_list_bert )
      realigned_streams = aligner.levenshtein_map()
      #print(realigned_streams[0])
      assert( (type( realigned_streams ) is tuple) and (len( realigned_streams ) == 2))
      ref_stream = merge_annotations( realigned_streams ) # effet de bord sur les annotations de ref du realigned stream
      print("Realignement Done!")

##Indexed Object to MWU
       
      text_dir = input_dir + file_lst[fnm_idx] + conf_parser.get( 'configuration', 'txt_sufx' )
      with open( text_dir, 'r', encoding = 'utf8' ) as ifdesc:
        text = ifdesc.read()
      
      doc = document( ident = file_lst[fnm_idx], content = text, metadata = 'metadata')
      
      indexed_object_to_mwu( doc, ref_stream, pointers_mwu, sentence_tokens_pointers )
      
      get_relations( doc, pointers_mwu, sentence_tokens_pointers )
       
      match_res = pattern_filter_svo_triplet( list(doc.rels.values()) )
      match_res_passive = pattern_passive_form_triplet( list(doc.rels.values()) )
      triplets[file_lst[fnm_idx]] = match_res + match_res_passive
      print("---------------------------------")
      print(file_lst[fnm_idx])    
      if ( fnm_idx <len(file_lst) ) :
          stanza_func.next_file()
          bert_func.next_file()
      

    with open(conf_parser.get( 'configuration', 'triplets' ), 'wb') as triplets_file:
                  pickle.dump(triplets, triplets_file)


    return True

  except:
    return False


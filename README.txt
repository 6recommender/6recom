We have two version of this software :
- Console application version via:
	- get_micro_topics.py ( to generate micro-topics)
	- 3 different ways to recommend articles :
		* W1 : recommendation_system_v4_without_specificity.py
		* W2 : recommendation_system_v5_specificityCorpus_specificityPositifs_specificityNegatifs.py
		* W3 : recommendation_system_v6_specificityCorpus_specificityInteresting.py

- Graphical user interface version via:
	- graphical_user_interface.py 
	(related to get_micro_topics_gui.py and recommendation_system_v6_gui.py (GUI version of W3))


* Use python >= 3.6.9
* To add the missing python modules : pip install module_name
( To find out missing modules, use the console application version rather than the GUI version )

* Download BERT: url: https://github.com/ROCmSoftwarePlatform/bert
* Download BERT models via : https://storage.googleapis.com/bert_models/2020_02_20/all_bert_models.zip
----------------------------------------------------------------
* Update the following paths in config.ini:
modeling / body_dir / global_output_dir /
recommendation_system_result_dir / background_image/
bert_config_file / init_checkpoint / vocab_file (Choose a bert model to use)
 
----------------------------------------------------------------
*!! The corpus files must be in text format. 
----------------------------------------------------------------
* To display the metadata of the document in the final result:
 - The data must be saved in a csv file with ' , ' as separator
 - The data must be in the following order : file_name,language,publication_date,authors,title

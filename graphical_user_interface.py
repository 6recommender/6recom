## This code written in 2020 by Maha BOUZAIENE <bouzaiene.maha@gmail.com> is licensed under CC BY-NC-SA
## This license allows reusers to distribute, remix, adapt, and build upon the material in any medium
## or format for noncommercial purposes only, and only so long as attribution is given to the creator.
## If you remix, adapt, or build upon the material, you must license the modified material under
## identical terms. 
## CC BY-NC-SA includes the following elements:
## BY – Credit must be given to the creator
## NC – Only noncommercial uses of the work are permitted
## SA – Adaptations must be shared under the same terms
## For further information see https://creativecommons.org/about/cclicenses/
## or look into the directory: ./copyright_NOTICES

import sys
from PyQt5.QtWidgets import *#QApplication, QWidget,QPushButton
import time
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from pathlib import Path
from configparser import ConfigParser


conf_parser = ConfigParser( allow_no_value = True )
conf_file = Path( __file__ ).parent / "config.ini"
conf_parser.read( conf_file )

class main_window(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        
        #self.setGeometry(200, 200, 1000, 700)
        self.setFixedSize(1000,700)
        self.setWindowTitle("6Recommender")
        #self.setStyleSheet("background-image : url(robot2.png);background-repeat: no-repeat;background-position: center")
        self.photo = QLabel(self)
        self.photo.resize(1000,700)
        self.photo.setPixmap(QPixmap(conf_parser.get( 'configuration', 'background_image' )))

        # creating a opacity effect
        self.opacity_effect = QGraphicsOpacityEffect()
  
        # setting opacity level
        self.opacity_effect.setOpacity(0.7)

##        # adding opacity effect to the label
##        self.photo.setGraphicsEffect(self.opacity_effect)
  
        
        self.label_main_window = QLabel(self)
        self.label_main_window.setText("6RECOMMENDER")
        self.label_main_window.resize(950,400)
        self.label_main_window.setAlignment(Qt.AlignCenter)
        self.label_main_window.setFont(QFont('Times', 60))
        self.label_main_window.setGraphicsEffect(self.opacity_effect)
        #self.label_main_window.move(50,30)
        #Button 1
        self.get_triplets_button = QPushButton(self)
        self.get_triplets_button.setText("MICRO-TOPICS")#µ
        self.get_triplets_button.resize(250,150)
        self.get_triplets_button.move(200,350)
        self.get_triplets_button.setStyleSheet("QPushButton { text-align: center; }")
        self.get_triplets_button.setFont(QFont('Times', 17))
        self.get_triplets_button.clicked.connect(self.get_triplets_button_clicked)

        self.get_triplets_button.setStyleSheet("QPushButton::hover"
                             "{"
                             "background-color : lightgreen;"
                             "}")
        #Button 2
        self.recommendation_system_button = QPushButton(self)
        self.recommendation_system_button.setText("RECOMMENDATION\n SYSTEM")
        self.recommendation_system_button.resize(260,150)
        self.recommendation_system_button.move(480,350)
        self.recommendation_system_button.setStyleSheet("QPushButton { text-align: center; }")
        self.recommendation_system_button.setFont(QFont('Times', 17))
        self.recommendation_system_button.clicked.connect(self.recommendation_system_button_clicked)

        self.recommendation_system_button.setStyleSheet("QPushButton::hover"
                             "{"
                             "background-color : lightgreen;"
                             "}")



        self.name_label = QLabel(self)
        self.name_label.setText("Written by Maha Bouzaiene, 2021")
        self.name_label.resize(300,50)
        self.name_label.setAlignment(Qt.AlignCenter)
        self.name_label.setFont(QFont('Times', 10))
        #self.name_label.move(700,640)
        self.name_label.move(330,620)
        self.name_label.setStyleSheet("QLabel"
                                    "{"
                                    #"border : 2px solid green;"
                                    "background : white;"
                                    "}")
        self.show()


    def get_triplets_button_clicked(self):
        self.win = get_triplet_window()
        self.win.show()
        self.close()

    def recommendation_system_button_clicked(self):
        self.win = recommendation_system_window()
        self.win.show()
        self.close()



class get_triplet_window(QDialog):
    def __init__(self):
        QDialog.__init__(self)
        #self.setGeometry(200, 200, 1000, 700)
        self.setFixedSize(1000,700)
        self.setWindowTitle('µTopics')


        self.photo = QLabel(self)
        self.photo.resize(1000,700)
        self.photo.setPixmap(QPixmap(conf_parser.get( 'configuration', 'background_image' )))
        
        
        self.return_button = QPushButton(self)
        self.return_button.setText("Return")
        self.return_button.resize(100,32)
        self.return_button.move(880,660)
        self.return_button.clicked.connect(self.go_main_window)


        self.launch_button = QPushButton(self)
        self.launch_button.setText("Find Micro-Topics")
        #self.launch_button.setGeometry(200, 150, 200, 100)
        #self.launch_button.setStyleSheet("border-radius : 50; border : 2px solid black")
        #self.launch_button.setStyleSheet("min-width: 10em;padding: 15px;")
        #self.launch_button.move(600,570)
        self.launch_button.setFont(QFont('Arial', 10))
        self.launch_button.clicked.connect(self.go_find_mico_topics_window)
        self.launch_button.setFixedHeight(50)
        self.launch_button.setStyleSheet("QPushButton::hover"
                             "{"
                             "background-color : lightblue;"
                             "}")


        self.directories_box()
        main_vertical_layout = QVBoxLayout( self )
        main_vertical_layout.addSpacing(-60)
        main_vertical_layout.addWidget( self.groupBox1 )
        main_vertical_layout.addSpacing(-20)
        main_vertical_layout.addWidget( self.launch_button )
        
        
    def directories_box(self):     

        self.label_corpus_directory = QLabel(self)
        self.label_corpus_directory.setText("Corpus Directory")
	
        self.textEdit_corpus_directory = QPlainTextEdit(str(conf_parser.get('configuration','corpus_dir')),self)
        self.textEdit_corpus_directory.resize(200,32)
#______________________________________________________________________________________________________________________________
        
        self.label_abstract_directory = QLabel(self)
        self.label_abstract_directory.setText("Abstract Directory")


        self.textEdit_abstract_directory = QPlainTextEdit(str(conf_parser.get('configuration','abstract_dir')),self)
        self.textEdit_abstract_directory.resize(200,32)

#______________________________________________________________________________________________________________________________
        
        self.triplets_directory_label = QLabel(self)
        self.triplets_directory_label.setText("Micro-Topics File")


        self.textEdit_triplets_directory = QPlainTextEdit(str(conf_parser.get('configuration','triplets')),self)
        self.textEdit_triplets_directory.resize(200,32)

#______________________________________________________________________________________________________________________________
        
        self.menu_button_label = QLabel(self)
        self.menu_button_label.setText("To use only the abstract of the document, please choose True \n Note that whether you choose True or False, the abstract directory MUST be different from the corpus directory")

        self.menu_button = QPushButton(self)
        self.menu_button.setText(str(conf_parser.get('configuration','extract_abstract')))
        self.menu_button.setFixedWidth(150)

#______________________________________________________________________________________________________________________________
        
        self.save_button = QPushButton(self)
        self.save_button.setText("Save")
        self.save_button.setFixedHeight(50)
        self.save_button.setFixedWidth(300)
        self.save_button.clicked.connect(self.save_button_directories_func)

#______________________________________________________________________________________________________________________________
        
        menu_list = ['True','False']
        self.choice = ''
        menu = QMenu()
        menu.triggered.connect(lambda x: self.save_data_menu(x.text()))
        self.menu_button.setMenu(menu)
        self.add_menu(menu_list, menu)

#______________________________________________________________________________________________________________________________

        self.corpus_dir_browser_button = QPushButton(self)
        self.corpus_dir_browser_button.setText("Browse")
        self.corpus_dir_browser_button.resize(100,32)
        self.corpus_dir_browser_button.clicked.connect(self.corpus_directory_browser_func)


        self.abstract_dir_browser_button = QPushButton(self)
        self.abstract_dir_browser_button.setText("Browse")
        self.abstract_dir_browser_button.resize(100,32)
        self.abstract_dir_browser_button.clicked.connect(self.abstract_directory_browser_func)


        self.triplets_dir_browser_button = QPushButton(self)
        self.triplets_dir_browser_button.setText("Browse")
        self.triplets_dir_browser_button.resize(100,32)
        self.triplets_dir_browser_button.clicked.connect(self.triplets_directory_browser_func)

#______________________________________________________________________________________________________________________________

        horizentalInnerLayout1 = QHBoxLayout()
        horizentalInnerLayout1.addWidget( self.textEdit_corpus_directory )
        horizentalInnerLayout1.addWidget( self.corpus_dir_browser_button )
        #------------------------------------------------------------------
        horizentalInnerLayout2 = QHBoxLayout()
        horizentalInnerLayout2.addWidget( self.textEdit_abstract_directory )
        horizentalInnerLayout2.addWidget( self.abstract_dir_browser_button )
        #------------------------------------------------------------------

        horizentalInnerLayout3 = QHBoxLayout()
        horizentalInnerLayout3.addWidget( self.textEdit_triplets_directory )
        horizentalInnerLayout3.addWidget( self.triplets_dir_browser_button )
        #------------------------------------------------------------------

        
        verticalInnerLayout = QVBoxLayout()
                        
        verticalInnerLayout.addWidget( self.label_corpus_directory )
        #verticalInnerLayout.addWidget( self.textEdit_corpus_directory )
        verticalInnerLayout.addLayout(horizentalInnerLayout1)
        verticalInnerLayout.addSpacing(60)
        
        verticalInnerLayout.addWidget( self.label_abstract_directory )
        #verticalInnerLayout.addWidget( self.textEdit_abstract_directory )
        verticalInnerLayout.addLayout(horizentalInnerLayout2)
        verticalInnerLayout.addSpacing(60)
        
        verticalInnerLayout.addWidget( self.triplets_directory_label )
        #verticalInnerLayout.addWidget( self.textEdit_triplets_directory )
        verticalInnerLayout.addLayout(horizentalInnerLayout3)
        verticalInnerLayout.addSpacing(60)
        
        verticalInnerLayout.addWidget( self.menu_button_label )
        verticalInnerLayout.addWidget( self.menu_button )
        verticalInnerLayout.addSpacing(30)
        verticalInnerLayout.addWidget( self.save_button,alignment=Qt.AlignCenter )#,alignment=Qt.AlignRight )
        
        self.groupBox1 = QGroupBox( "Add Directories" )
        self.groupBox1.setFixedHeight(500)
        self.groupBox1.setFont(QFont('Arial', 10))
        self.groupBox1.setLayout( verticalInnerLayout )
        #----------------------------------------------
        op=QGraphicsOpacityEffect(self.groupBox1)
        op.setOpacity(1.00)
        self.groupBox1.setGraphicsEffect(op)
        self.groupBox1.setAutoFillBackground(True)
        #----------------------------------------------


        
    def save_button_directories_func(self):
        self.corpus_directory = self.textEdit_corpus_directory.toPlainText()
        self.abstract_directory = self.textEdit_abstract_directory.toPlainText()
        self.triplets_directory = self.textEdit_triplets_directory.toPlainText()
        self.extract_abstract = self.choice
        
        if str(self.corpus_directory) != '':
            conf_parser.set('configuration','corpus_dir',str(self.corpus_directory))
        if str(self.abstract_directory) != '':
            conf_parser.set('configuration','abstract_dir',str(self.abstract_directory))
        if str(self.triplets_directory) != '':
            conf_parser.set('configuration','triplets',str(self.triplets_directory))
        if str(self.extract_abstract) != '':
            conf_parser.set('configuration','extract_abstract',str(self.extract_abstract))

        with open(conf_file, 'w', encoding = 'utf8' ) as f:
            conf_parser.write(f)

        self.textEdit_corpus_directory.setPlainText(conf_parser.get('configuration','corpus_dir'))
        self.textEdit_abstract_directory.setPlainText(conf_parser.get('configuration','abstract_dir'))
        self.textEdit_triplets_directory.setPlainText(conf_parser.get('configuration','triplets'))
        
    def add_menu(self, data, menu_obj):
        if isinstance(data, dict):
            for k, v in data.items():
                sub_menu = QMenu(k, menu_obj)
                menu_obj.addMenu(sub_menu)
                self.add_menu(v, sub_menu)
        elif isinstance(data, list):
            for element in data:
                self.add_menu(element, menu_obj)
        else:
            action = menu_obj.addAction(data)
            action.setIconVisibleInMenu(True)

        

    def save_data_menu(self,choice):
        self.choice = choice
        self.menu_button.setText(self.choice)

    def corpus_directory_browser_func(self):
        fileName = QFileDialog.getExistingDirectory(self, "Select Folder")
        if fileName:
            self.textEdit_corpus_directory.setPlainText( fileName + '/' )

    def abstract_directory_browser_func(self):
        fileName = QFileDialog.getExistingDirectory(self, "Select Folder")
        if fileName:
            self.textEdit_abstract_directory.setPlainText( fileName + '\\' )

    def triplets_directory_browser_func(self):
        fileName,_ = QFileDialog.getOpenFileName(self, 'Select a File')
        if fileName:
            self.textEdit_triplets_directory.setPlainText( fileName )


    def go_find_mico_topics_window(self):   
        self.displayW = micro_topics_found_window()
        self.displayW.show()
        self.close()


    def go_main_window(self):
        self.mainW = main_window()
        self.mainW.show()
        self.close()


class micro_topics_found_window(QDialog):
    def __init__(self):

        QDialog.__init__(self)
        self.setGeometry(200, 200, 1000, 700)
        self.setWindowTitle('Find_micro_topics')

        self.photo = QLabel(self)
        self.photo.resize(1000,700)
        self.photo.setPixmap(QPixmap(conf_parser.get( 'configuration', 'background_image' )))

        self.return_button = QPushButton(self)
        self.return_button.setText("Return")
        self.return_button.resize(100,32)
        self.return_button.move(880,660)
        self.return_button.clicked.connect(self.go_find_micro_topics_window)


        self.find_mico_topics()
        
        main_vertical_layout = QGridLayout( self )
        main_vertical_layout.addWidget( self.result_display ,1,1)
        #self.setLayout(main_vertical_layout)

    def find_mico_topics(self):
        import importlib
        import get_micro_topics_gui
        importlib.reload(get_micro_topics_gui)

        from get_micro_topics_gui import get_micro_topics
        is_done = get_micro_topics()
        self.result_display = QTextEdit(self)
        self.result_display.setReadOnly(True)
        self.result_display.setFont(QFont('Times', 20))
        
        self.result_display.setFixedHeight(90)
        self.result_display.setFixedWidth(500)
        
        if is_done:
            self.result_display.setPlainText('Done')
        else:
            self.result_display.setPlainText('Oops!! Check execution problems')
            

        self.result_display.setAlignment(Qt.AlignCenter)
        op=QGraphicsOpacityEffect(self.result_display)
        op.setOpacity(1.00)
        self.result_display.setGraphicsEffect(op)
        self.result_display.setAutoFillBackground(True)

    def go_find_micro_topics_window(self):
        self.mainW = get_triplet_window()
        self.mainW.show()
        self.close()


class recommendation_system_window(QDialog):
    
    def __init__(self):

        QDialog.__init__(self)
        #self.setGeometry(200, 200, 1000, 700)
        self.setFixedSize(1000,700)
        self.setWindowTitle('Recommendation System')

        self.photo = QLabel(self)
        self.photo.resize(1000,700)
        self.photo.setPixmap(QPixmap(conf_parser.get( 'configuration', 'background_image' )))
        

        self.return_button = QPushButton(self)
        self.return_button.setText("Return")
        self.return_button.resize(100,32)
        self.return_button.move(880,660)
        self.return_button.clicked.connect(self.go_main_window)



        self.launch_button = QPushButton(self)
        self.launch_button.setText("Recommend Articles")
        self.launch_button.setGeometry(200, 150, 200, 100)
        #self.launch_button.setStyleSheet("border-radius : 50; border : 2px solid black")
        self.launch_button.setStyleSheet("min-width: 10em;padding: 15px;")
        self.launch_button.move(600,500)
        self.launch_button.setFont(QFont('Arial', 10))
        self.launch_button.clicked.connect(self.go_recommendation_system_window)

        self.launch_button.setFixedHeight(50)
        self.launch_button.setStyleSheet("QPushButton::hover"
                             "{"
                             "background-color : lightblue;"
                             "}")

#___________________________________________________________________________________________________________
        
        self.directories_box()
        self.parameters_box()
        main_vertical_layout = QVBoxLayout( self )
        main_horizental_layout = QHBoxLayout( self )
        main_vertical_layout.addWidget( self.groupBox1 )
        #main_vertical_layout.addWidget( self.groupBox2 )
        main_horizental_layout.addWidget( self.groupBox2 )
        main_horizental_layout.addWidget( self.launch_button )
        main_vertical_layout.addLayout(main_horizental_layout)
        
#-----------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------        
    def directories_box(self):
        self.label_triplets_directory = QLabel(self)
        self.label_triplets_directory.setText("Micro-Topics File")
        self.label_triplets_directory.move(50,30)
        
	
        self.textEdit_triplets_directory = QPlainTextEdit(str(conf_parser.get('configuration','triplets')),self)
        self.textEdit_triplets_directory.move(50,50)
        self.textEdit_triplets_directory.resize(400,32)
        
#___________________________________________________________________________________________________________
        
        self.label_expert_triplets_directory = QLabel(self)
        self.label_expert_triplets_directory.setText("expert Micro-Topics File")
        self.label_expert_triplets_directory.move(50,130)

        self.textEdit_expert_triplets_directory = QPlainTextEdit(str(conf_parser.get('configuration','triplets_expert')),self)
        self.textEdit_expert_triplets_directory.move(50,150)
        self.textEdit_expert_triplets_directory.resize(400,32)

#___________________________________________________________________________________________________________
        
        self.label_uninteresting_triplets_directory = QLabel(self)
        self.label_uninteresting_triplets_directory.setText("uninteresting Micro-Topics File")
        self.label_uninteresting_triplets_directory.move(50,230)

        self.textEdit_uninteresting_triplets_directory = QPlainTextEdit(str(conf_parser.get('configuration','uninteresting_triplets')),self)
        self.textEdit_uninteresting_triplets_directory.move(50,250)
        self.textEdit_uninteresting_triplets_directory.resize(400,32)

#___________________________________________________________________________________________________________
        
        self.label_original_texts_directory = QLabel(self)
        self.label_original_texts_directory.setText("Original Texts Directory")
        self.label_original_texts_directory.move(50,330)

        self.textEdit_original_texts_directory = QPlainTextEdit(str(conf_parser.get('configuration','corpus_dir')),self)
        self.textEdit_original_texts_directory.move(50,350)
        self.textEdit_original_texts_directory.resize(400,32)

#___________________________________________________________________________________________________________
        
        self.label_metadata_file_directory = QLabel(self)
        self.label_metadata_file_directory.setText("Metadata File")
        self.label_metadata_file_directory.move(50,430)

        self.textEdit_metadata_file_directory = QPlainTextEdit(str(conf_parser.get('configuration','metadata_file')),self)
        self.textEdit_metadata_file_directory.move(50,450)
        self.textEdit_metadata_file_directory.resize(400,32)

#___________________________________________________________________________________________________________

        self.save_button_directories = QPushButton(self)
        self.save_button_directories.setText("Save")
        self.save_button_directories.setFixedWidth(100)
        self.save_button_directories.clicked.connect(self.save_button_directories_func)

#___________________________________________________________________________________________________________
        self.micro_themes_browser_button = QPushButton(self)
        self.micro_themes_browser_button.setText("Browse")
        self.micro_themes_browser_button.resize(100,32)
        self.micro_themes_browser_button.clicked.connect(self.micro_themes_browser_func)


        self.micro_themes_expert_browser_button = QPushButton(self)
        self.micro_themes_expert_browser_button.setText("Browse")
        self.micro_themes_expert_browser_button.resize(100,32)
        self.micro_themes_expert_browser_button.clicked.connect(self.micro_themes_expert_browser_func)


        self.micro_themes_uninteresting_browser_button = QPushButton(self)
        self.micro_themes_uninteresting_browser_button.setText("Browse")
        self.micro_themes_uninteresting_browser_button.resize(100,32)
        self.micro_themes_uninteresting_browser_button.clicked.connect(self.micro_themes_uninteresting_browser_func)


        self.original_text_browser_button = QPushButton(self)
        self.original_text_browser_button.setText("Browse")
        self.original_text_browser_button.resize(100,32)
        self.original_text_browser_button.clicked.connect(self.micro_themes_original_browser_func)

        self.metadata_file_browser_button = QPushButton(self)
        self.metadata_file_browser_button.setText("Browse")
        self.metadata_file_browser_button.resize(100,32)
        self.metadata_file_browser_button.clicked.connect(self.micro_themes_metadata_browser_func)

#______________________________________________________________________________________________________________________________
        
        self.menu_button_label = QLabel(self)
        self.menu_button_label.setText("If a metadata file with the required format is not available, please choose False")

        self.menu_button = QPushButton(self)
        self.menu_button.setText(str(conf_parser.get('configuration','have_metadata_file')))
        self.menu_button.setFixedWidth(150)


#___________________________________________________________________________________________________________
        menu_list = ['True','False']
        self.choice = ''
        menu = QMenu()
        menu.triggered.connect(lambda x: self.save_data_menu(x.text()))
        self.menu_button.setMenu(menu)
        self.add_menu(menu_list, menu)


#___________________________________________________________________________________________________________

        verticalInnerLayout = QVBoxLayout()
        horizentalInnerLayout1 = QHBoxLayout()
        horizentalInnerLayout1.addWidget(self.textEdit_triplets_directory)
        horizentalInnerLayout1.addWidget(self.micro_themes_browser_button)

        horizentalInnerLayout2 = QHBoxLayout()
        horizentalInnerLayout2.addWidget(self.textEdit_expert_triplets_directory)
        horizentalInnerLayout2.addWidget(self.micro_themes_expert_browser_button)

        horizentalInnerLayout3 = QHBoxLayout()
        horizentalInnerLayout3.addWidget(self.textEdit_uninteresting_triplets_directory)
        horizentalInnerLayout3.addWidget(self.micro_themes_uninteresting_browser_button)

        horizentalInnerLayout4 = QHBoxLayout()
        horizentalInnerLayout4.addWidget(self.textEdit_original_texts_directory)
        horizentalInnerLayout4.addWidget(self.original_text_browser_button)

        horizentalInnerLayout5 = QHBoxLayout()
        horizentalInnerLayout5.addWidget(self.textEdit_metadata_file_directory)
        horizentalInnerLayout5.addWidget(self.metadata_file_browser_button)

        
        verticalInnerLayout.addWidget( self.label_triplets_directory )
        #verticalInnerLayout.addWidget( self.textEdit_triplets_directory )
        verticalInnerLayout.addLayout(horizentalInnerLayout1)
        
        verticalInnerLayout.addWidget( self.label_expert_triplets_directory )
        #verticalInnerLayout.addWidget( self.textEdit_expert_triplets_directory )
        verticalInnerLayout.addLayout(horizentalInnerLayout2)
        
        verticalInnerLayout.addWidget( self.label_uninteresting_triplets_directory )
        #verticalInnerLayout.addWidget( self.textEdit_uninteresting_triplets_directory )
        verticalInnerLayout.addLayout(horizentalInnerLayout3)
        
        verticalInnerLayout.addWidget( self.label_original_texts_directory )
        #verticalInnerLayout.addWidget( self.textEdit_original_texts_directory )
        verticalInnerLayout.addLayout(horizentalInnerLayout4)
        
        verticalInnerLayout.addWidget( self.label_metadata_file_directory )
        #verticalInnerLayout.addWidget( self.textEdit_metadata_file_directory )
        verticalInnerLayout.addLayout(horizentalInnerLayout5)

        verticalInnerLayout.addWidget( self.menu_button_label )
        verticalInnerLayout.addWidget( self.menu_button )

        verticalInnerLayout.addWidget( self.save_button_directories,alignment=Qt.AlignRight )

        self.groupBox1 = QGroupBox( "Add Directories" )
        self.groupBox1.setFixedHeight(500)
        self.groupBox1.setFont(QFont('Arial', 10))
        self.groupBox1.setLayout( verticalInnerLayout )

        #----------------------------------------------
        op=QGraphicsOpacityEffect(self.groupBox1)
        op.setOpacity(1.00)
        self.groupBox1.setGraphicsEffect(op)
        self.groupBox1.setAutoFillBackground(True)
        #----------------------------------------------

#------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------
        
    def parameters_box(self):
        self.label_discrimination_threshold = QLabel(self)
        self.label_discrimination_threshold.setText("Discrimination Threshold")
        self.label_discrimination_threshold.move(50,30)
	
        self.textEdit_discrimination_threshold = QPlainTextEdit(str(conf_parser.get('configuration','discrimination_threshold')),self)
        self.textEdit_discrimination_threshold.move(50,50)
        self.textEdit_discrimination_threshold.resize(400,32)

        
#___________________________________________________________________________________________________________
        
        self.label_filtering_threshold = QLabel(self)
        self.label_filtering_threshold.setText("Filtering Threshold")
        self.label_filtering_threshold.move(50,30)
	
        self.textEdit_filtering_threshold = QPlainTextEdit(str(conf_parser.get('configuration','filtering_threshold')),self)
        self.textEdit_filtering_threshold.move(50,50)
        self.textEdit_filtering_threshold.resize(400,32)
        
#___________________________________________________________________________________________________________
        
        self.label_specificity_threshold = QLabel(self)
        self.label_specificity_threshold.setText("Specificity Threshold")
        self.label_specificity_threshold.move(50,30)
        
        self.textEdit_specificity_threshold = QPlainTextEdit(str(conf_parser.get('configuration','specificity_threshold')),self)
        self.textEdit_specificity_threshold.move(50,50)
        self.textEdit_specificity_threshold.resize(400,32)
        
#___________________________________________________________________________________________________________

        self.save_button_parameters = QPushButton(self)
        self.save_button_parameters.setText("Save")
        self.save_button_parameters.setFixedWidth(100)
        self.save_button_parameters.clicked.connect(self.save_button_parameters_func)
 
#___________________________________________________________________________________________________________

        horizentalInnerLayout = QHBoxLayout()
        verticalInnerLayout1 = QVBoxLayout()
        verticalInnerLayout2 = QVBoxLayout()
        verticalInnerLayout3 = QVBoxLayout()
        verticalInnerLayout4 = QVBoxLayout()
        main_verticalInnerLayout = QVBoxLayout()
        
        verticalInnerLayout1.addWidget( self.label_discrimination_threshold )
        verticalInnerLayout1.addWidget( self.textEdit_discrimination_threshold )
        horizentalInnerLayout.addLayout(verticalInnerLayout1)
        
        verticalInnerLayout2.addWidget( self.label_filtering_threshold )
        verticalInnerLayout2.addWidget( self.textEdit_filtering_threshold )
        horizentalInnerLayout.addLayout(verticalInnerLayout2)
        
        verticalInnerLayout3.addWidget( self.label_specificity_threshold )
        verticalInnerLayout3.addWidget( self.textEdit_specificity_threshold )
        horizentalInnerLayout.addLayout(verticalInnerLayout3)

        main_verticalInnerLayout.addLayout(horizentalInnerLayout)
        
        main_verticalInnerLayout.addWidget( self.save_button_parameters,alignment=Qt.AlignRight )



        self.groupBox2 = QGroupBox( "Set Parameters" )
        self.groupBox2.setFixedHeight(150)
        self.groupBox2.setFixedWidth(600)
        self.groupBox2.setFont(QFont('Arial', 10))
        self.groupBox2.setLayout( main_verticalInnerLayout )

        #----------------------------------------------
        op=QGraphicsOpacityEffect(self.groupBox2)
        op.setOpacity(1.00)
        self.groupBox2.setGraphicsEffect(op)
        self.groupBox2.setAutoFillBackground(True)
        #----------------------------------------------


    def save_button_directories_func(self):
        self.triplets_directory = self.textEdit_triplets_directory.toPlainText()
        self.expert_triplets_directory = self.textEdit_expert_triplets_directory.toPlainText()
        self.uninteresting_triplets_directory = self.textEdit_uninteresting_triplets_directory.toPlainText()
        self.original_texts_directory = self.textEdit_original_texts_directory.toPlainText()
        self.metadata_file_directory = self.textEdit_metadata_file_directory.toPlainText()
        self.have_metadataFile = self.choice
        if str(self.triplets_directory) != '' :
            conf_parser.set('configuration','triplets',str(self.triplets_directory))
        if str(self.expert_triplets_directory) != '' :
            conf_parser.set('configuration','triplets_expert',str(self.expert_triplets_directory))
        if str(self.uninteresting_triplets_directory) != '':
            conf_parser.set('configuration','uninteresting_triplets',str(self.uninteresting_triplets_directory))
        if str(self.original_texts_directory) != '':
            conf_parser.set('configuration','corpus_dir',str(self.original_texts_directory))
        if str(self.metadata_file_directory) != '':
            conf_parser.set('configuration','metadata_file',str(self.metadata_file_directory))
        if str(self.have_metadataFile) != '':
            conf_parser.set('configuration','have_metadata_file',str(self.have_metadataFile))


        with open(conf_file, 'w', encoding = 'utf8' ) as f:
            conf_parser.write(f)

        self.textEdit_triplets_directory.setPlainText( conf_parser.get('configuration','triplets') )
        self.textEdit_expert_triplets_directory.setPlainText( conf_parser.get('configuration','triplets_expert') )
        self.textEdit_uninteresting_triplets_directory.setPlainText( conf_parser.get('configuration','uninteresting_triplets') )
        self.textEdit_original_texts_directory.setPlainText( conf_parser.get('configuration','corpus_dir') )
        self.textEdit_metadata_file_directory.setPlainText( conf_parser.get('configuration','metadata_file') )

    def save_button_parameters_func(self):
        self.discrimination_threshold = self.textEdit_discrimination_threshold.toPlainText()
        self.filtering_threshold = self.textEdit_filtering_threshold.toPlainText()
        self.specificity_threshold = self.textEdit_specificity_threshold.toPlainText()
        if str(self.discrimination_threshold) != '':
            conf_parser.set('configuration','discrimination_threshold',str(self.discrimination_threshold))
        if str(self.filtering_threshold) != '':
            conf_parser.set('configuration','filtering_threshold',str(self.filtering_threshold))
        if str(self.specificity_threshold) != '':
            conf_parser.set('configuration','specificity_threshold',str(self.specificity_threshold))

        with open(conf_file, 'w', encoding = 'utf8' ) as f:
            conf_parser.write(f)

        self.textEdit_discrimination_threshold.setPlainText( conf_parser.get('configuration','discrimination_threshold') )
        self.textEdit_filtering_threshold.setPlainText( conf_parser.get('configuration','filtering_threshold') )
        self.textEdit_specificity_threshold.setPlainText( conf_parser.get('configuration','specificity_threshold') )


    def micro_themes_browser_func(self):
        fileName,_ = QFileDialog.getOpenFileName(self, 'Select a File')
        if fileName:
            self.textEdit_triplets_directory.setPlainText( fileName )

    def micro_themes_expert_browser_func(self):
        fileName,_ = QFileDialog.getOpenFileName(self, 'Select a File')
        if fileName:
            self.textEdit_expert_triplets_directory.setPlainText( fileName )

    def micro_themes_uninteresting_browser_func(self):
        fileName,_ = QFileDialog.getOpenFileName(self, 'Select a File')
        if fileName:
            self.textEdit_uninteresting_triplets_directory.setPlainText( fileName )

    def micro_themes_original_browser_func(self):
        fileName = QFileDialog.getExistingDirectory(self, "Select Folder")
        if fileName:
            self.textEdit_original_texts_directory.setPlainText( fileName + '\\' )


    def micro_themes_metadata_browser_func(self):
        fileName,_ = QFileDialog.getOpenFileName(self, 'Select a File')
        if fileName:
            self.textEdit_metadata_file_directory.setPlainText( fileName )



    def add_menu(self, data, menu_obj):
        if isinstance(data, dict):
            for k, v in data.items():
                sub_menu = QMenu(k, menu_obj)
                menu_obj.addMenu(sub_menu)
                self.add_menu(v, sub_menu)
        elif isinstance(data, list):
            for element in data:
                self.add_menu(element, menu_obj)
        else:
            action = menu_obj.addAction(data)
            action.setIconVisibleInMenu(True)

        

    def save_data_menu(self,choice):
        self.choice = choice
        self.menu_button.setText(self.choice)


    def go_recommendation_system_window(self):
        self.displayW = result_display_window()
        self.displayW.show()
        self.close()
        
    def go_main_window(self):
        self.mainW = main_window()
        self.mainW.show()
        self.close()

class result_display_window(QDialog):
    
    def __init__(self):
        QDialog.__init__(self)
        self.setGeometry(200, 200, 1000, 700)
        self.setWindowTitle('Recommendation System Result')
        
        self.photo = QLabel(self)
        self.photo.resize(1000,700)
        self.photo.setPixmap(QPixmap(conf_parser.get( 'configuration', 'background_image' )))

        self.return_button = QPushButton(self)
        self.return_button.setText("Return")
        self.return_button.resize(100,32)
        self.return_button.move(880,660)
        self.return_button.clicked.connect(self.return_recommendation_system_window)


        self.recommendation_system()
        main_vertical_layout = QGridLayout( self )
        main_vertical_layout.addWidget( self.result_display ,1,1)
    def recommendation_system(self):
        import importlib
        import recommendation_system_v6_gui
        importlib.reload(recommendation_system_v6_gui)

        from recommendation_system_v6_gui import article_recommendation,write_sentence,get_source_text
        
        articles_to_recommend,dict_metadata = article_recommendation()
        result_text = ''
        for article in list(articles_to_recommend.keys()):
            list_sentences = []
            articles_to_recommend[article].sort(key=lambda tup: tup[1],reverse=True)
            
            for tr in articles_to_recommend[article]:            
                triplet,cos_sim = tr
                sentence = write_sentence(triplet)[0]
                sentence = get_source_text(triplet, conf_parser.get('configuration','corpus_dir'), article, conf_parser.get( 'configuration', 'txt_sufx' ))

                if not any( sentence in x for x in list_sentences ):
                    list_sentences.append((sentence,cos_sim))
     
            t1 = 'Article {0} is recommended '.format(article)
            if dict_metadata != None:
        #--------------"metadata_by_article_corpus_hal.csv / metadata_dnn.csv / metadata_hydrate_slurry.csv"-----------------------------------
            
                t2 = '           Title: {0}'.format(dict_metadata[article][2])
                t3 = '           Authors: {0}'.format(dict_metadata[article][1])
                t4 = '           Published in: {0}'.format(dict_metadata[article][0])

        #--------------"globaleACL.csv"-------------------------------------------------------
        ##        print('           Title: {0}'.format(dict_metadata[article][2]))
        ##        print('           Published in: {0}'.format(dict_metadata[article][1]))
                t = t1 + '\n' + t2 + '\n' + t3 + '\n' + t4 + '\n'
            else:
                t = t1 + '\n'

            
            for se in list_sentences:
                t5 = '           {0}'.format(se)
                t = t + '\n' +t5

            t = t + '\n'
            result_text = result_text + '\n' + '---------------------------------------' + '\n' + t

        self.result_display = QPlainTextEdit(self)
        self.result_display.setPlainText(result_text)
        #sauvegarder le resultat dans un fichier texte
        result_file_dir = conf_parser.get('configuration','recommendation_system_result_dir')
        with open(result_file_dir, 'w', encoding = 'utf8' ) as res_f:
            res_f.write(result_text)

        self.result_display.setReadOnly(True)
        self.result_display.setFont(QFont('Arial', 10))
        self.result_display.setFixedHeight(550)

        op=QGraphicsOpacityEffect(self.result_display)
        op.setOpacity(1.00)
        self.result_display.setGraphicsEffect(op)
        self.result_display.setAutoFillBackground(True)

    def return_recommendation_system_window(self):
        self.displayW = recommendation_system_window()
        self.displayW.show()
        self.close()



app = QApplication.instance() 
if not app:
    app = QApplication(sys.argv)
app.setStyle('Fusion')    
fen = main_window()


app.exec_()

## This code written in 2020 by Maha BOUZAIENE <bouzaiene.maha@gmail.com> is licensed under CC BY-NC-SA
## This license allows reusers to distribute, remix, adapt, and build upon the material in any medium
## or format for noncommercial purposes only, and only so long as attribution is given to the creator.
## If you remix, adapt, or build upon the material, you must license the modified material under
## identical terms. 
## CC BY-NC-SA includes the following elements:
## BY – Credit must be given to the creator
## NC – Only noncommercial uses of the work are permitted
## SA – Adaptations must be shared under the same terms
## For further information see https://creativecommons.org/about/cclicenses/
## or look into the directory: ./copyright_NOTICES

import os
import ntpath
from pathlib import Path
import shutil
from configparser import ConfigParser
directoryFichierConfig = Path( __file__ ).parent / "config.ini"
parser = ConfigParser( allow_no_value = True )
parser.read( directoryFichierConfig )

def getListOfFiles(dirName,listOfFiles):
   listOfFile = os.listdir(dirName)
   allFiles = list()   
   
   for entry in listOfFile:
      fullPath = os.path.join(dirName, entry)
      if os.path.isdir(fullPath):
          allFiles = allFiles + getListOfFiles(fullPath,listOfFiles)
      else:
          allFiles.append(fullPath)
          
   list_of_files = open( listOfFiles, 'w', encoding = 'utf8' )
   for l in allFiles:
      l = os.path.basename(l)
      l = os.path.splitext(l)[0]
      list_of_files.write(l+"\n")
   list_of_files.close()
               
   return allFiles



## This code written in 2020 by Maha BOUZAIENE <bouzaiene.maha@gmail.com> is licensed under CC BY-NC-SA
## This license allows reusers to distribute, remix, adapt, and build upon the material in any medium
## or format for noncommercial purposes only, and only so long as attribution is given to the creator.
## If you remix, adapt, or build upon the material, you must license the modified material under
## identical terms. 
## CC BY-NC-SA includes the following elements:
## BY – Credit must be given to the creator
## NC – Only noncommercial uses of the work are permitted
## SA – Adaptations must be shared under the same terms
## For further information see https://creativecommons.org/about/cclicenses/
## or look into the directory: ./copyright_NOTICES

from dynprogalign import indexed_object
from pathlib import Path
from configparser import ConfigParser
from text_span_v2 import mwu, relation, construction, construction_pattern, document
from unification import *
from unification.match import *
from pprint import pprint
from copy import deepcopy
directoryFichierConfig = Path( __file__ ).parent / "config.ini"
conf_parser = ConfigParser( allow_no_value = True )
conf_parser.read( directoryFichierConfig )


def merge_annotations( aligned_streams, annotation_mixer = lambda aref, ahyp : aref ):
    # ce n'est pas la peine de recréer un indexed_object, il suffit d'ajouter les annotations des symboles hypothese
    # aux annotation du symbole refersence avec lequel ils sont associés
    (ref_stream, hyp_stream) = aligned_streams
    for i in range( 0, len( ref_stream )):
#        print("****************************")
#        print(ref_stream[ i ])
        aligned_symbol_tuple = ref_stream[ i ] 
#        print( type( aligned_symbol_tuple ))
        #pprint( aligned_symbol_tuple )
#        print( len( aligned_symbol_tuple ) )
        (ref_isym, ref_atom_span_lst, hyp_isym_span_lst, hyp_atom_span_lst ) = aligned_symbol_tuple
        assert( type( ref_isym ) is indexed_object )
#        print( 'merging annotation of ref symbol {0} whose annotations are {1}'.format( ref_isym, ref_isym.annotations))
        merged_hyp_annotations = {}
        compteur = 0
        for hyp_isym_span in hyp_isym_span_lst:
            for hyp_isym in hyp_isym_span:
                compteur+=1
#                print( '\t\twith hyp symbol {0} annotations {1}'.format( hyp_isym, hyp_isym.annotations ))
                assert( type( hyp_isym ) is indexed_object )
                for k in hyp_isym.annotations.keys():
                    if (k not in merged_hyp_annotations.keys()):
                        merged_hyp_annotations[k] = [0 for x in range(int(conf_parser.get('configuration','max_seq_length')))]
                    merged_hyp_annotations[k] = [x + y for x, y in zip(merged_hyp_annotations[k], hyp_isym.annotations[k])]
        for k in merged_hyp_annotations.keys():
            if compteur != 0:
                merged_hyp_annotations[k] = [x/compteur for x in merged_hyp_annotations[k]]
        
        for k in merged_hyp_annotations.keys():
            if k not in ref_isym.annotations.keys():
                ref_isym.annotations[ k ] = merged_hyp_annotations[ k ]
            else:
                ref_isym.annotations[ k ] = annotation_mixer( ref_isym.annotations[ k ], merged_hyp_annotations[ k ] )
#    print( '=====merged annotation of ref symbol {0} whose annotations are {1}'.format( ref_isym, ref_isym.annotations))
    return ref_stream # le flux d'alignement contient maintenant par effet de bord les annotations d'hypothèse qui ont été fusionnées avec les annotations de ref



def indexed_object_to_mwu( doc, realigned_streams, pointers_mwu, sentence_tokens_pointers ):
    sentence_tokens_pointers[ doc.id ] = {}
    pointers_mwu[ doc.id ] = {}
    for i in range( len( realigned_streams ) ):
        tok = realigned_streams[ i ][ 0 ]
        tok_mwu = mwu( nm = doc.id + '_Tok_' + str( tok.annotations[ 'id' ] ) + '_'+ str( tok.annotations[ 'sent_id' ] ),
                        txtsps = [ ( tok.annotations[ 'start_char' ], tok.annotations[ 'end_char' ] ) ],
                        annotations = tok.annotations,
                        typ = "Token" )

        doc.add_mwu( tok_mwu )
    
        pointers_mwu[ doc.id ][ tok_mwu.name ] = tok_mwu
        sent_id = tok.annotations[ 'sent_id' ]
        if( sent_id not in sentence_tokens_pointers[ doc.id ].keys() ):
            sentence_tokens_pointers[ doc.id ][ sent_id ] = []
        sentence_tokens_pointers[ doc.id ][ sent_id ].append( tok_mwu.name )

def get_relations(doc,pointers_mwu,sentence_tokens_pointers):
    for k in sentence_tokens_pointers[doc.id].keys():
#        print("======================================================")
        for mwu_pointer in sentence_tokens_pointers[doc.id][k]:
            w_heads = pointers_mwu[doc.id][mwu_pointer].annotations["w_head"]
            for idx in range(len(w_heads)):
                if w_heads[idx] == 0 :
                    nm_rel = '_Rel_'+mwu_pointer+'_'+mwu_pointer
                    rel = relation(nm =nm_rel,src=pointers_mwu[doc.id][mwu_pointer], 
                                   trg=pointers_mwu[doc.id][mwu_pointer],annotations = {},
                                   typ=pointers_mwu[doc.id][mwu_pointer].annotations["w_deprel"][idx])
#                    print()
#                    print('SRC:{0} : {1} ({2}) ||  TRG:{3} : {4} ({5}) || type: {6}'.format(rel.src.name, 
#                        pointers_mwu[doc.id][rel.src.name].annotations['text'], pointers_mwu[doc.id][rel.src.name].annotations['w_upos'], 
#                        rel.trgt.name, pointers_mwu[doc.id][rel.trgt.name].annotations['text'], 
#                        pointers_mwu[doc.id][rel.trgt.name].annotations['w_upos'],rel.typ))
                    doc.add_rel(rel)
                else:
                    for mwuP in sentence_tokens_pointers[doc.id][k]:
                        if ( pointers_mwu[doc.id][mwuP].annotations["id"] == w_heads[idx] ):
                            nm_rel = '_Rel_'+mwu_pointer+'_'+mwuP

                            rel = relation(nm =nm_rel,src=pointers_mwu[doc.id][mwu_pointer], 
                                           trg=pointers_mwu[doc.id][mwuP],annotations = {},
                                           typ=pointers_mwu[doc.id][mwu_pointer].annotations["w_deprel"][idx])
                            
#                            print()
#                            print('SRC:{0} : {1} ({2}) ||  TRG:{3} : {4} ({5}) || type: {6}'.format(rel.src.name, 
#                                pointers_mwu[doc.id][rel.src.name].annotations['text'], 
#                                pointers_mwu[doc.id][rel.src.name].annotations['w_upos'], rel.trgt.name, 
#                                pointers_mwu[doc.id][rel.trgt.name].annotations['text'], pointers_mwu[doc.id][rel.trgt.name].annotations['w_upos'],rel.typ))
                            doc.add_rel(rel)



def pattern_filter_svo_triplet( relation_set ):
    suj_mwu_var  = var('subject')
    verb_mwu_var = var('verb')
    obj_mwu_var  = var('object')
    aux_mwu_var = var('verb_aux')
    subj_adj_mwu_var = var('subj_adj_mwu_var')
    obj_adj_mwu_var = var('obj_adj_mwu_var')
    subj_noun_mwu_var = var('subj_noun_mwu_var')
    obj_noun_mwu_var = var('obj_noun_mwu_var')
    compound_obj_mwu_var = var('compound_obj_mwu_var')
    compound_subj_mwu_var = var('compound_subj_mwu_var')
    annot_var_1 = var( 'annotations1')
    annot_var_2 = var( 'annotations2')
    annot_var_3 = var( 'annotations3')
    annot_var_4 = var('annotations4')
    annot_var_5 = var('annotations5')
    annot_var_6 = var('annotations6')
    annot_var_7 = var('annotations7')
    annot_var_8 = var('annotations8')
    annot_var_9 = var('annotations9')
    rel_suj_verb_name = var('rel_suj_verb_name')
    rel_verb_obj_name = var('rel_verb_obj_name')
    rel_verb_aux_name = var('rel_verb_aux_name')
    rel_subj_adj_name = var('rel_subj_adj_name')
    rel_obj_adj_name = var('rel_obj_adj_name')
    rel_subj_noun_name = var('rel_subj_noun_name')
    rel_obj_noun_name = var('rel_obj_noun_name')
    rel_compound_obj_name = var('rel_compound_obj_name')
    rel_compound_subj_name = var('rel_compound_subj_name')

    rel_suj_verb = relation( nm = rel_suj_verb_name,
                             src = suj_mwu_var,
                             trg = verb_mwu_var,
                             annotations = annot_var_1,
                             typ='nsubj' )
    rel_verb_obj = relation( nm = rel_verb_obj_name,
                             src = obj_mwu_var,
                             trg = verb_mwu_var,
                             annotations = annot_var_2,
                             typ='obj' )



    rel_subj_adj = relation( nm = rel_subj_adj_name,
                            src = subj_adj_mwu_var,
                            trg = suj_mwu_var,
                            annotations = annot_var_4,
                            typ='amod' )

    rel_subj_noun = relation( nm = rel_subj_noun_name,
                            src = subj_noun_mwu_var,
                            trg = suj_mwu_var,
                            annotations = annot_var_6,
                            typ='nmod' )

    rel_compound_subj = relation( nm = rel_compound_subj_name,
                            src = compound_subj_mwu_var,
                            trg = suj_mwu_var,
                            annotations = annot_var_9,
                            typ='compound' )




    rel_obj_adj = relation( nm = rel_obj_adj_name,
                             src = obj_adj_mwu_var,
                             trg = obj_mwu_var,
                             annotations = annot_var_5, 
                             typ='amod' )

    rel_obj_noun = relation( nm = rel_obj_noun_name,
                             src = obj_noun_mwu_var,
                             trg = obj_mwu_var,
                             annotations = annot_var_7, 
                             typ='nmod' )


    rel_compound_obj = relation( nm = rel_compound_obj_name,
                             src = compound_obj_mwu_var,
                             trg = obj_mwu_var,
                             annotations = annot_var_8, 
                             typ='compound' )



#------------------------------------------------------------------------------------
#--------------------------------MOD_SUBJ_ADJ----------------------------------------
    rel_subj_adj_adj_name = var('rel_subj_adj_adj_name')
    subj_adj_adj_mwu_var = var('subj_adj_adj_mwu_var')
    rel_subj_adj_noun_name = var('rel_subj_adj_noun_name')
    subj_adj_noun_mwu_var = var('subj_adj_noun_mwu_var')
    rel_subj_adj_compound_name = var('rel_subj_adj_compound_name')
    subj_adj_compound_mwu_var = var('subj_adj_compound_mwu_var')
    
    annot_var_13 = var('annot_var_13')
    annot_var_14 = var('annot_var_14')
    annot_var_15 = var('annot_var_15')


    rel_subj_adj_adj = relation( nm = rel_subj_adj_adj_name,
                             src = subj_adj_adj_mwu_var,
                             trg = subj_adj_mwu_var,
                             annotations = annot_var_13,
                             typ='amod' )

    rel_subj_adj_noun = relation( nm = rel_subj_adj_noun_name,
                             src = subj_adj_noun_mwu_var,
                             trg = subj_adj_mwu_var,
                             annotations = annot_var_14,
                             typ='nmod' )

    rel_subj_adj_compound = relation( nm = rel_subj_adj_compound_name,
                             src = subj_adj_compound_mwu_var,
                             trg = subj_adj_mwu_var,
                             annotations = annot_var_15,
                             typ='compound' )
#-------------------------------------------------------------------------------------
#-------------------------------MOD_SUBJ_noun-----------------------------------------
    rel_subj_noun_adj_name = var('rel_subj_noun_adj_name')
    subj_noun_adj_mwu_var = var('subj_noun_adj_mwu_var')
    rel_subj_noun_noun_name = var('rel_subj_noun_noun_name')
    subj_noun_noun_mwu_var = var('subj_noun_noun_mwu_var')
    rel_subj_noun_compound_name = var('rel_subj_noun_compound_name')
    subj_noun_compound_mwu_var = var('subj_noun_compound_mwu_var')

    annot_var_16 = var('annot_var_16')
    annot_var_17 = var('annot_var_17')
    annot_var_18 = var('annot_var_18')


    rel_subj_noun_adj = relation( nm = rel_subj_noun_adj_name,
                             src = subj_noun_adj_mwu_var,
                             trg = subj_noun_mwu_var,
                             annotations = annot_var_16,
                             typ='amod' )

    rel_subj_noun_noun = relation( nm = rel_subj_noun_noun_name,
                             src = subj_noun_noun_mwu_var,
                             trg = subj_noun_mwu_var,
                             annotations = annot_var_17,
                             typ='nmod' )

    rel_subj_noun_compound = relation( nm = rel_subj_noun_compound_name,
                             src = subj_noun_compound_mwu_var,
                             trg = subj_noun_mwu_var,
                             annotations = annot_var_18,
                             typ='compound' )

#-------------------------------------------------------------------------------------
#-------------------------------MOD_SUBJ_compound-----------------------------------------
    rel_subj_compound_adj_name = var('rel_subj_compound_adj_name')
    subj_compound_adj_mwu_var = var('subj_compound_adj_mwu_var')
    rel_subj_compound_noun_name = var('rel_subj_compound_noun_name')
    subj_compound_noun_mwu_var = var('subj_compound_noun_mwu_var')
    rel_subj_compound_compound_name = var('rel_subj_compound_compound_name')
    subj_compound_compound_mwu_var = var('subj_compound_compound_mwu_var')

    annot_var_19 = var('annot_var_19')
    annot_var_20 = var('annot_var_20')
    annot_var_21 = var('annot_var_21')


    rel_subj_compound_adj = relation( nm = rel_subj_compound_adj_name,
                             src = subj_compound_adj_mwu_var,
                             trg = compound_subj_mwu_var,
                             annotations = annot_var_19,
                             typ='amod' )

    rel_subj_compound_noun = relation( nm = rel_subj_compound_noun_name,
                             src = subj_compound_noun_mwu_var,
                             trg = compound_subj_mwu_var,
                             annotations = annot_var_20,
                             typ='nmod' )

    rel_subj_compound_compound = relation( nm = rel_subj_compound_compound_name,
                             src = subj_compound_compound_mwu_var,
                             trg = compound_subj_mwu_var,
                             annotations = annot_var_21,
                             typ='compound' )


#------------------------------------------------------------------------------------
#--------------------------------MOD_OBJ_ADJ----------------------------------------
    rel_obj_adj_adj_name = var('rel_obj_adj_adj_name')
    obj_adj_adj_mwu_var = var('obj_adj_adj_mwu_var')
    rel_obj_adj_noun_name = var('rel_obj_adj_noun_name')
    obj_adj_noun_mwu_var = var('obj_adj_noun_mwu_var')
    rel_obj_adj_compound_name = var('rel_obj_adj_compound_name')
    obj_adj_compound_mwu_var = var('obj_adj_compound_mwu_var')

    annot_var_22 = var('annot_var_22')
    annot_var_23 = var('annot_var_23')
    annot_var_24 = var('annot_var_24')


    rel_obj_adj_adj = relation( nm = rel_obj_adj_adj_name,
                             src = obj_adj_adj_mwu_var,
                             trg = obj_adj_mwu_var,
                             annotations = annot_var_22,
                             typ='amod' )

    rel_obj_adj_noun = relation( nm = rel_obj_adj_noun_name,
                             src = obj_adj_noun_mwu_var,
                             trg = obj_adj_mwu_var,
                             annotations = annot_var_23,
                             typ='nmod' )

    rel_obj_adj_compound = relation( nm = rel_obj_adj_compound_name,
                             src = obj_adj_compound_mwu_var,
                             trg = obj_adj_mwu_var,
                             annotations = annot_var_24,
                             typ='compound' )

#-------------------------------------------------------------------------------------
#-------------------------------MOD_OBJ_noun-----------------------------------------
    rel_obj_noun_adj_name = var('rel_obj_noun_adj_name')
    obj_noun_adj_mwu_var = var('obj_noun_adj_mwu_var')
    rel_obj_noun_noun_name = var('rel_obj_noun_noun_name')
    obj_noun_noun_mwu_var = var('obj_noun_noun_mwu_var')
    rel_obj_noun_compound_name = var('rel_obj_noun_compound_name')
    obj_noun_compound_mwu_var = var('obj_noun_compound_mwu_var')

    annot_var_25 = var('annot_var_25')
    annot_var_26 = var('annot_var_26')
    annot_var_27 = var('annot_var_27')


    rel_obj_noun_adj = relation( nm = rel_obj_noun_adj_name,
                             src = obj_noun_adj_mwu_var,
                             trg = obj_noun_mwu_var,
                             annotations = annot_var_25,
                             typ='amod' )

    rel_obj_noun_noun = relation( nm = rel_obj_noun_noun_name,
                             src = obj_noun_noun_mwu_var,
                             trg = obj_noun_mwu_var,
                             annotations = annot_var_26,
                             typ='nmod' )

    rel_obj_noun_compound = relation( nm = rel_obj_noun_compound_name,
                             src = obj_noun_compound_mwu_var,
                             trg = obj_noun_mwu_var,
                             annotations = annot_var_27,
                             typ='compound' )

#-------------------------------------------------------------------------------------
#-------------------------------MOD_OBJ_compound-----------------------------------------
    rel_obj_compound_adj_name = var('rel_obj_compound_adj_name')
    obj_compound_adj_mwu_var = var('obj_compound_adj_mwu_var')
    rel_obj_compound_noun_name = var('rel_obj_compound_noun_name')
    obj_compound_noun_mwu_var = var('obj_compound_noun_mwu_var')
    rel_obj_compound_compound_name = var('rel_obj_compound_compound_name')
    obj_compound_compound_mwu_var = var('obj_compound_compound_mwu_var')

    annot_var_28 = var('annot_var_28')
    annot_var_29 = var('annot_var_29')
    annot_var_30 = var('annot_var_30')


    rel_obj_compound_adj = relation( nm = rel_obj_compound_adj_name,
                             src = obj_compound_adj_mwu_var,
                             trg = compound_obj_mwu_var,
                             annotations = annot_var_28,
                             typ='amod' )

    rel_obj_compound_noun = relation( nm = rel_obj_compound_noun_name,
                             src = obj_compound_noun_mwu_var,
                             trg = compound_obj_mwu_var,
                             annotations = annot_var_29,
                             typ='nmod' )

    rel_obj_compound_compound = relation( nm = rel_obj_compound_compound_name,
                             src = obj_compound_compound_mwu_var,
                             trg = compound_obj_mwu_var,
                             annotations = annot_var_30,
                             typ='compound' )


#-------------------------------------------------------------------------------------

    
    pattern_triplet_SVO_mods__kstruct = construction_pattern( nm = 'pattern_triplet_SVO_mods', 
                                                              rel_lst = { rel_suj_verb.name:rel_suj_verb, rel_verb_obj.name: rel_verb_obj },
                                                              rel_lst_opt = { rel_obj_adj.name:rel_obj_adj , rel_obj_noun.name:rel_obj_noun, rel_compound_obj.name:rel_compound_obj,
                                                                  rel_subj_adj.name:rel_subj_adj, rel_subj_noun.name:rel_subj_noun, rel_compound_subj.name:rel_compound_subj} )
   
#--------------------------------------------------------------------SUBJ_MODS----------------------------------------------------------------------------------------

    
    pattern_triplet_S_ADJ_mods_kstruct = construction_pattern( nm = 'pattern_triplet_S_ADJ_mods',
                                                              rel_lst = { rel_subj_adj.name:rel_subj_adj },
                                                              rel_lst_opt = { rel_subj_adj_adj.name:rel_subj_adj_adj, rel_subj_adj_noun.name:rel_subj_adj_noun, 
                                                                  rel_subj_adj_compound.name:rel_subj_adj_compound} )



    pattern_triplet_S_NOUN_mods_kstruct = construction_pattern( nm = 'pattern_triplet_S_NOUN_mods',
                                                              rel_lst = { rel_subj_noun.name:rel_subj_noun },
                                                              rel_lst_opt = { rel_subj_noun_adj.name:rel_subj_noun_adj, rel_subj_noun_noun.name:rel_subj_noun_noun,
                                                                  rel_subj_noun_compound.name:rel_subj_noun_compound} )

    pattern_triplet_S_compound_mods_kstruct = construction_pattern( nm = 'pattern_triplet_S_compound_mods',
                                                              rel_lst = { rel_compound_subj.name:rel_compound_subj },
                                                              rel_lst_opt = { rel_subj_compound_adj.name:rel_subj_compound_adj, rel_subj_compound_noun.name:rel_subj_compound_noun,
                                                                  rel_subj_compound_compound.name:rel_subj_compound_compound} )



#--------------------------------------------------------------------OBJ_MODS----------------------------------------------------------------------------------------


    pattern_triplet_O_ADJ_mods_kstruct = construction_pattern( nm = 'pattern_triplet_O_ADJ_mods',
                                                              rel_lst = { rel_obj_adj.name:rel_obj_adj },
                                                              rel_lst_opt = { rel_obj_adj_adj.name:rel_obj_adj_adj, rel_obj_adj_noun.name:rel_obj_adj_noun,
                                                                  rel_obj_adj_compound.name:rel_obj_adj_compound} )



    pattern_triplet_O_NOUN_mods_kstruct = construction_pattern( nm = 'pattern_triplet_O_NOUN_mods',
                                                              rel_lst = { rel_obj_noun.name:rel_obj_noun },
                                                              rel_lst_opt = { rel_obj_noun_adj.name:rel_obj_noun_adj, rel_obj_noun_noun.name:rel_obj_noun_noun,
                                                                  rel_obj_noun_compound.name:rel_obj_noun_compound} )

    pattern_triplet_O_compound_mods_kstruct = construction_pattern( nm = 'pattern_triplet_O_compound_mods',
                                                              rel_lst = { rel_compound_obj.name:rel_compound_obj },
                                                              rel_lst_opt = { rel_obj_compound_adj.name:rel_obj_compound_adj, rel_obj_compound_noun.name:rel_obj_compound_noun,
                                                                  rel_obj_compound_compound.name:rel_obj_compound_compound} )


#------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pattern_list = [pattern_triplet_S_ADJ_mods_kstruct,pattern_triplet_S_NOUN_mods_kstruct,pattern_triplet_S_compound_mods_kstruct,
                    pattern_triplet_O_ADJ_mods_kstruct,pattern_triplet_O_NOUN_mods_kstruct,pattern_triplet_O_compound_mods_kstruct]   

    updated_relation_set = relation_set

    res = []
    res_optimal = []
    first_triplet_found = False

    #Find the first triplet
    env1 = pattern_triplet_SVO_mods__kstruct.search_relation_list( updated_relation_set,env = {} )

    if (env1 and (type(env1) is dict)):
        first_triplet_found = True
        for pattern in pattern_list:
            env2 = pattern.search_relation_list( updated_relation_set,env = env1 )
            if (env2 and (type(env2) is dict)):
                env1 = deepcopy(env2)

        env1_str = deepcopy(env1)
        list_keys = list(env1_str.keys())
        for k in list_keys:
            new_key = str(k).replace('~','')
            env1_str[new_key] = env1_str.pop(k)


        res.append(env1_str)
        tmp = {}
        for k,v in env1_str.items():
            if type(v) is mwu :
                tmp.update({k:v.annotations['text']})
        if len(list(tmp.keys()))!=0:
            res_optimal.append(tmp)


    while env1:
        first_triplet_mods_found = False
        env1 = pattern_triplet_SVO_mods__kstruct.next_search_relation_list(env={})
        
        if (env1 and (type(env1) is dict) and (first_triplet_found == False)):
            first_triplet_found = True
            first_triplet_mods_found = True
            for pattern in pattern_list:
                env2 = pattern.search_relation_list( updated_relation_set,env = env1 )
                if (env2 and (type(env2) is dict)):
                    env1 = deepcopy(env2)

            env1_str = deepcopy(env1)
            list_keys = list(env1_str.keys())
            for k in list_keys:
                new_key = str(k).replace('~','')
                env1_str[new_key] = env1_str.pop(k)

            res.append(env1_str)
            tmp = {}
            for k,v in env1_str.items():
                if type(v) is mwu :
                    tmp.update({k:v.annotations['text']})
            if len(list(tmp.keys()))!=0:
                res_optimal.append(tmp)

        
        if (env1 and (type(env1) is dict) and (first_triplet_mods_found == False)):
            for pattern in pattern_list:
                env2 = pattern.next_search_relation_list( env = env1 )
                if (env2 and (type(env2) is dict)):
                    env1 = deepcopy(env2)

            env1_str = deepcopy(env1)
            list_keys = list(env1_str.keys())
            for k in list_keys:
                new_key = str(k).replace('~','')
                env1_str[new_key] = env1_str.pop(k)

            res.append(env1_str)
            tmp = {}
            for k,v in env1_str.items():
                if type(v) is mwu :
                    tmp.update({k:v.annotations['text']})
            if len(list(tmp.keys()))!=0:
                res_optimal.append(tmp)
        

    print('res_optimal')
    pprint(res_optimal)    
    return res




def pattern_passive_form_triplet( relation_set ):
    suj_mwu_var  = var('subject')
    verb_mwu_var = var('verb')
    obj_mwu_var  = var('object')
    aux_mwu_var = var('verb_aux')
    subj_pass_adj_mwu_var = var('obj_adj_mwu_var')
    obj_pass_adj_mwu_var = var('subj_adj_mwu_var')
    subj_pass_noun_mwu_var = var('obj_noun_mwu_var')
    obj_pass_noun_mwu_var = var('subj_noun_mwu_var')
    compound_obj_pass_mwu_var = var('compound_subj_mwu_var')
    compound_subj_pass_mwu_var = var('compound_obj_mwu_var')
    annot_var_1 = var( 'annotations1')
    annot_var_2 = var( 'annotations2')
    annot_var_3 = var( 'annotations3')
    annot_var_4 = var('annotations4')
    annot_var_5 = var('annotations5')
    annot_var_6 = var('annotations6')
    annot_var_7 = var('annotations7')
    annot_var_8 = var('annotations8')
    annot_var_9 = var('annotations9')
    rel_suj_pass_verb_name = var('rel_obj_verb_name')
    rel_verb_obj_pass_name = var('rel_verb_subj_name')
    rel_subj_pass_adj_name = var('rel_obj_adj_name')
    rel_obj_pass_adj_name = var('rel_subj_adj_name')
    rel_subj_pass_noun_name = var('rel_obj_noun_name')
    rel_obj_pass_noun_name = var('rel_subj_noun_name')
    rel_compound_obj_pass_name = var('rel_compound_subj_name')
    rel_compound_subj_pass_name = var('rel_compound_obj_name')

    rel_suj_pass_verb = relation( nm = rel_suj_pass_verb_name,
                             src = obj_mwu_var,
                             trg = verb_mwu_var,
                             annotations = annot_var_1,
                             typ='nsubj:pass' )
    rel_verb_obj_pass = relation( nm = rel_verb_obj_pass_name,
                             src = suj_mwu_var,
                             trg = verb_mwu_var,
                             annotations = annot_var_2,
                             typ='obl' )

    rel_subj_pass_adj = relation( nm = rel_subj_pass_adj_name,
                            src = subj_pass_adj_mwu_var,
                            trg = obj_mwu_var,
                            annotations = annot_var_4,
                            typ='amod' )

    rel_subj_pass_noun = relation( nm = rel_subj_pass_noun_name,
                            src = subj_pass_noun_mwu_var,
                            trg = obj_mwu_var,
                            annotations = annot_var_6,
                            typ='nmod' )

    rel_compound_subj_pass = relation( nm = rel_compound_subj_pass_name,
                            src = compound_subj_pass_mwu_var,
                            trg = obj_mwu_var,
                            annotations = annot_var_9,
                            typ='compound' )




    rel_obj_pass_adj = relation( nm = rel_obj_pass_adj_name,
                             src = obj_pass_adj_mwu_var,
                             trg = suj_mwu_var,
                             annotations = annot_var_5,
                             typ='amod' )

    rel_obj_pass_noun = relation( nm = rel_obj_pass_noun_name,
                             src = obj_pass_noun_mwu_var,
                             trg = suj_mwu_var,
                             annotations = annot_var_7,
                             typ='nmod' )


    rel_compound_obj_pass = relation( nm = rel_compound_obj_pass_name,
                             src = compound_obj_pass_mwu_var,
                             trg = suj_mwu_var,
                             annotations = annot_var_8,
                             typ='compound' )



#------------------------------------------------------------------------------------
#--------------------------------MOD_SUBJ_ADJ----------------------------------------
    rel_subj_pass_adj_adj_name = var('rel_subj_pass_adj_adj_name')
    subj_pass_adj_adj_mwu_var = var('subj_pass_adj_adj_mwu_var')
    rel_subj_pass_adj_noun_name = var('rel_subj_pass_adj_noun_name')
    subj_pass_adj_noun_mwu_var = var('subj_pass_adj_noun_mwu_var')
    rel_subj_pass_adj_compound_name = var('rel_subj_pass_adj_compound_name')
    subj_pass_adj_compound_mwu_var = var('subj_pass_adj_compound_mwu_var')
    
    annot_var_13 = var('annot_var_13')
    annot_var_14 = var('annot_var_14')
    annot_var_15 = var('annot_var_15')


    rel_subj_pass_adj_adj = relation( nm = rel_subj_pass_adj_adj_name,
                             src = subj_pass_adj_adj_mwu_var,
                             trg = subj_pass_adj_mwu_var,
                             annotations = annot_var_13,
                             typ='amod' )

    rel_subj_pass_adj_noun = relation( nm = rel_subj_pass_adj_noun_name,
                             src = subj_pass_adj_noun_mwu_var,
                             trg = subj_pass_adj_mwu_var,
                             annotations = annot_var_14,
                             typ='nmod' )

    rel_subj_pass_adj_compound = relation( nm = rel_subj_pass_adj_compound_name,
                             src = subj_pass_adj_compound_mwu_var,
                             trg = subj_pass_adj_mwu_var,
                             annotations = annot_var_15,
                             typ='compound' )
#-------------------------------------------------------------------------------------
#-------------------------------MOD_SUBJ_noun-----------------------------------------
    rel_subj_pass_noun_adj_name = var('rel_subj_pass_noun_adj_name')
    subj_pass_noun_adj_mwu_var = var('subj_pass_noun_adj_mwu_var')
    rel_subj_pass_noun_noun_name = var('rel_subj_pass_noun_noun_name')
    subj_pass_noun_noun_mwu_var = var('subj_pass_noun_noun_mwu_var')
    rel_subj_pass_noun_compound_name = var('rel_subj_pass_noun_compound_name')
    subj_pass_noun_compound_mwu_var = var('subj_pass_noun_compound_mwu_var')

    annot_var_16 = var('annot_var_16')
    annot_var_17 = var('annot_var_17')
    annot_var_18 = var('annot_var_18')


    rel_subj_pass_noun_adj = relation( nm = rel_subj_pass_noun_adj_name,
                             src = subj_pass_noun_adj_mwu_var,
                             trg = subj_pass_noun_mwu_var,
                             annotations = annot_var_16,
                             typ='amod' )

    rel_subj_pass_noun_noun = relation( nm = rel_subj_pass_noun_noun_name,
                             src = subj_pass_noun_noun_mwu_var,
                             trg = subj_pass_noun_mwu_var,
                             annotations = annot_var_17,
                             typ='nmod' )

    rel_subj_pass_noun_compound = relation( nm = rel_subj_pass_noun_compound_name,
                             src = subj_pass_noun_compound_mwu_var,
                             trg = subj_pass_noun_mwu_var,
                             annotations = annot_var_18,
                             typ='compound' )

#-------------------------------------------------------------------------------------
#-------------------------------MOD_SUBJ_compound-----------------------------------------
    rel_subj_pass_compound_adj_name = var('rel_subj_pass_compound_adj_name')
    subj_pass_compound_adj_mwu_var = var('subj_pass_compound_adj_mwu_var')
    rel_subj_pass_compound_noun_name = var('rel_subj_pass_compound_noun_name')
    subj_pass_compound_noun_mwu_var = var('subj_pass_compound_noun_mwu_var')
    rel_subj_pass_compound_compound_name = var('rel_subj_pass_compound_compound_name')
    subj_pass_compound_compound_mwu_var = var('subj_pass_compound_compound_mwu_var')

    annot_var_19 = var('annot_var_19')
    annot_var_20 = var('annot_var_20')
    annot_var_21 = var('annot_var_21')


    rel_subj_pass_compound_adj = relation( nm = rel_subj_pass_compound_adj_name,
                             src = subj_pass_compound_adj_mwu_var,
                             trg = compound_subj_pass_mwu_var,
                             annotations = annot_var_19,
                             typ='amod' )

    rel_subj_pass_compound_noun = relation( nm = rel_subj_pass_compound_noun_name,
                             src = subj_pass_compound_noun_mwu_var,
                             trg = compound_subj_pass_mwu_var,
                             annotations = annot_var_20,
                             typ='nmod' )

    rel_subj_pass_compound_compound = relation( nm = rel_subj_pass_compound_compound_name,
                             src = subj_pass_compound_compound_mwu_var,
                             trg = compound_subj_pass_mwu_var,
                             annotations = annot_var_21,
                             typ='compound' )


#------------------------------------------------------------------------------------
#--------------------------------MOD_OBJ_ADJ----------------------------------------
    rel_obj_pass_adj_adj_name = var('rel_obj_pass_adj_adj_name')
    obj_pass_adj_adj_mwu_var = var('obj_pass_adj_adj_mwu_var')
    rel_obj_pass_adj_noun_name = var('rel_obj_pass_adj_noun_name')
    obj_pass_adj_noun_mwu_var = var('obj_pass_adj_noun_mwu_var')
    rel_obj_pass_adj_compound_name = var('rel_obj_pass_adj_compound_name')
    obj_pass_adj_compound_mwu_var = var('obj_pass_adj_compound_mwu_var')

    annot_var_22 = var('annot_var_22')
    annot_var_23 = var('annot_var_23')
    annot_var_24 = var('annot_var_24')


    rel_obj_pass_adj_adj = relation( nm = rel_obj_pass_adj_adj_name,
                             src = obj_pass_adj_adj_mwu_var,
                             trg = obj_pass_adj_mwu_var,
                             annotations = annot_var_22,
                             typ='amod' )

    rel_obj_pass_adj_noun = relation( nm = rel_obj_pass_adj_noun_name,
                             src = obj_pass_adj_noun_mwu_var,
                             trg = obj_pass_adj_mwu_var,
                             annotations = annot_var_23,
                             typ='nmod' )

    rel_obj_pass_adj_compound = relation( nm = rel_obj_pass_adj_compound_name,
                             src = obj_pass_adj_compound_mwu_var,
                             trg = obj_pass_adj_mwu_var,
                             annotations = annot_var_24,
                             typ='compound' )

#-------------------------------------------------------------------------------------
#-------------------------------MOD_OBJ_noun-----------------------------------------
    rel_obj_pass_noun_adj_name = var('rel_obj_pass_noun_adj_name')
    obj_pass_noun_adj_mwu_var = var('obj_pass_noun_adj_mwu_var')
    rel_obj_pass_noun_noun_name = var('rel_obj_pass_noun_noun_name')
    obj_pass_noun_noun_mwu_var = var('obj_pass_noun_noun_mwu_var')
    rel_obj_pass_noun_compound_name = var('rel_obj_pass_noun_compound_name')
    obj_pass_noun_compound_mwu_var = var('obj_pass_noun_compound_mwu_var')

    annot_var_25 = var('annot_var_25')
    annot_var_26 = var('annot_var_26')
    annot_var_27 = var('annot_var_27')


    rel_obj_pass_noun_adj = relation( nm = rel_obj_pass_noun_adj_name,
                             src = obj_pass_noun_adj_mwu_var,
                             trg = obj_pass_noun_mwu_var,
                             annotations = annot_var_25,
                             typ='amod' )

    rel_obj_pass_noun_noun = relation( nm = rel_obj_pass_noun_noun_name,
                             src = obj_pass_noun_noun_mwu_var,
                             trg = obj_pass_noun_mwu_var,
                             annotations = annot_var_26,
                             typ='nmod' )

    rel_obj_pass_noun_compound = relation( nm = rel_obj_pass_noun_compound_name,
                             src = obj_pass_noun_compound_mwu_var,
                             trg = obj_pass_noun_mwu_var,
                             annotations = annot_var_27,
                             typ='compound' )

#-------------------------------------------------------------------------------------
#-------------------------------MOD_OBJ_compound-----------------------------------------
    rel_obj_pass_compound_adj_name = var('rel_obj_pass_compound_adj_name')
    obj_pass_compound_adj_mwu_var = var('obj_pass_compound_adj_mwu_var')
    rel_obj_pass_compound_noun_name = var('rel_obj_pass_compound_noun_name')
    obj_pass_compound_noun_mwu_var = var('obj_pass_compound_noun_mwu_var')
    rel_obj_pass_compound_compound_name = var('rel_obj_pass_compound_compound_name')
    obj_pass_compound_compound_mwu_var = var('obj_pass_compound_compound_mwu_var')

    annot_var_28 = var('annot_var_28')
    annot_var_29 = var('annot_var_29')
    annot_var_30 = var('annot_var_30')


    rel_obj_pass_compound_adj = relation( nm = rel_obj_pass_compound_adj_name,
                             src = obj_pass_compound_adj_mwu_var,
                             trg = compound_obj_pass_mwu_var,
                             annotations = annot_var_28,
                             typ='amod' )

    rel_obj_pass_compound_noun = relation( nm = rel_obj_pass_compound_noun_name,
                             src = obj_pass_compound_noun_mwu_var,
                             trg = compound_obj_pass_mwu_var,
                             annotations = annot_var_29,
                             typ='nmod' )

    rel_obj_pass_compound_compound = relation( nm = rel_obj_pass_compound_compound_name,
                             src = obj_pass_compound_compound_mwu_var,
                             trg = compound_obj_pass_mwu_var,
                             annotations = annot_var_30,
                             typ='compound' )


#-------------------------------------------------------------------------------------



    pattern_triplet_SVO_mods__kstruct = construction_pattern( nm = 'pattern_triplet_SVO_mods',
                                                              rel_lst = { rel_suj_pass_verb.name:rel_suj_pass_verb, rel_verb_obj_pass.name: rel_verb_obj_pass },
                                                              rel_lst_opt = { rel_obj_pass_adj.name:rel_obj_pass_adj , rel_obj_pass_noun.name:rel_obj_pass_noun,
                                                                  rel_compound_obj_pass.name:rel_compound_obj_pass,rel_subj_pass_adj.name:rel_subj_pass_adj,
                                                                  rel_subj_pass_noun.name:rel_subj_pass_noun, rel_compound_subj_pass.name:rel_compound_subj_pass} )



#--------------------------------------------------------------------SUBJ_MODS----------------------------------------------------------------------------------------

    
    pattern_triplet_S_pass_ADJ_mods_kstruct = construction_pattern( nm = 'pattern_triplet_S_pass_ADJ_mods',
                                                              rel_lst = { rel_subj_pass_adj.name:rel_subj_pass_adj },
                                                              rel_lst_opt = { rel_subj_pass_adj_adj.name:rel_subj_pass_adj_adj,
                                                                              rel_subj_pass_adj_noun.name:rel_subj_pass_adj_noun, 
                                                                              rel_subj_pass_adj_compound.name:rel_subj_pass_adj_compound} )



    pattern_triplet_S_pass_NOUN_mods_kstruct = construction_pattern( nm = 'pattern_triplet_S_pass_NOUN_mods',
                                                              rel_lst = { rel_subj_pass_noun.name:rel_subj_pass_noun },
                                                              rel_lst_opt = { rel_subj_pass_noun_adj.name:rel_subj_pass_noun_adj,
                                                                              rel_subj_pass_noun_noun.name:rel_subj_pass_noun_noun,
                                                                              rel_subj_pass_noun_compound.name:rel_subj_pass_noun_compound} )

    pattern_triplet_S_pass_compound_mods_kstruct = construction_pattern( nm = 'pattern_triplet_S_pass_compound_mods',
                                                              rel_lst = { rel_compound_subj_pass.name:rel_compound_subj_pass },
                                                              rel_lst_opt = { rel_subj_pass_compound_adj.name:rel_subj_pass_compound_adj,
                                                                              rel_subj_pass_compound_noun.name:rel_subj_pass_compound_noun,
                                                                              rel_subj_pass_compound_compound.name:rel_subj_pass_compound_compound} )



#--------------------------------------------------------------------OBJ_MODS----------------------------------------------------------------------------------------


    pattern_triplet_O_pass_ADJ_mods_kstruct = construction_pattern( nm = 'pattern_triplet_O_pass_ADJ_mods',
                                                              rel_lst = { rel_obj_pass_adj.name:rel_obj_pass_adj },
                                                              rel_lst_opt = { rel_obj_pass_adj_adj.name:rel_obj_pass_adj_adj,
                                                                              rel_obj_pass_adj_noun.name:rel_obj_pass_adj_noun,
                                                                              rel_obj_pass_adj_compound.name:rel_obj_pass_adj_compound} )



    pattern_triplet_O_pass_NOUN_mods_kstruct = construction_pattern( nm = 'pattern_triplet_O_pass_NOUN_mods',
                                                              rel_lst = { rel_obj_pass_noun.name:rel_obj_pass_noun },
                                                              rel_lst_opt = { rel_obj_pass_noun_adj.name:rel_obj_pass_noun_adj,
                                                                              rel_obj_pass_noun_noun.name:rel_obj_pass_noun_noun,
                                                                              rel_obj_pass_noun_compound.name:rel_obj_pass_noun_compound} )

    pattern_triplet_O_pass_compound_mods_kstruct = construction_pattern( nm = 'pattern_triplet_O_pass_compound_mods',
                                                              rel_lst = { rel_compound_obj_pass.name:rel_compound_obj_pass },
                                                              rel_lst_opt = { rel_obj_pass_compound_adj.name:rel_obj_pass_compound_adj,
                                                                              rel_obj_pass_compound_noun.name:rel_obj_pass_compound_noun,
                                                                              rel_obj_pass_compound_compound.name:rel_obj_pass_compound_compound} )


#------------------------------------------------------------------------------------------------------------------------------------------------------------------
    pattern_list = [pattern_triplet_S_pass_ADJ_mods_kstruct,pattern_triplet_S_pass_NOUN_mods_kstruct,pattern_triplet_S_pass_compound_mods_kstruct,
                    pattern_triplet_O_pass_ADJ_mods_kstruct,pattern_triplet_O_pass_NOUN_mods_kstruct,pattern_triplet_O_pass_compound_mods_kstruct]   




    updated_relation_set = relation_set

    res = []
    res_optimal = []
    first_triplet_found = False
    
    #Find the first triplet
    
    env1 = pattern_triplet_SVO_mods__kstruct.search_relation_list( updated_relation_set,env = {} )
            
    if (env1 and (type(env1) is dict)):
        first_triplet_found = True
        for pattern in pattern_list:
            env2 = pattern.search_relation_list( updated_relation_set,env = env1 )
            if (env2 and (type(env2) is dict)):
                env1 = deepcopy(env2)
        
        env1_str = deepcopy(env1)
        list_keys = list(env1_str.keys())
        for k in list_keys:
            new_key = str(k).replace('~','')
            env1_str[new_key] = env1_str.pop(k)


        res.append(env1_str)
        tmp = {}
        for k,v in env1_str.items():
            if type(v) is mwu :
                tmp.update({k:v.annotations['text']})
        if len(list(tmp.keys()))!=0:
            res_optimal.append(tmp)


    while env1:
        first_triplet_mods_found = False
        env1 = pattern_triplet_SVO_mods__kstruct.next_search_relation_list(env={})
        
        if (env1 and (type(env1) is dict) and (first_triplet_found == False)):
            first_triplet_found = True
            first_triplet_mods_found = True
            for pattern in pattern_list:
                env2 = pattern.search_relation_list( updated_relation_set,env = env1 )
                if (env2 and (type(env2) is dict)):
                    env1 = deepcopy(env2)
            env1_str = deepcopy(env1)
            list_keys = list(env1_str.keys())
            for k in list_keys:
                new_key = str(k).replace('~','')
                env1_str[new_key] = env1_str.pop(k)

            res.append(env1_str)
            tmp = {}
            for k,v in env1_str.items():
                if type(v) is mwu :
                    tmp.update({k:v.annotations['text']})
            if len(list(tmp.keys()))!=0:
                res_optimal.append(tmp)

        
        if (env1 and (type(env1) is dict) and (first_triplet_mods_found == False)):
            for pattern in pattern_list:
                env2 = pattern.next_search_relation_list( env = env1 )
                if (env2 and (type(env2) is dict)):
                    env1 = deepcopy(env2)

            env1_str = deepcopy(env1)
            list_keys = list(env1_str.keys())
            for k in list_keys:
                new_key = str(k).replace('~','')
                env1_str[new_key] = env1_str.pop(k)

            res.append(env1_str)
            tmp = {}
            for k,v in env1_str.items():
                if type(v) is mwu :
                    tmp.update({k:v.annotations['text']})
            if len(list(tmp.keys()))!=0:
                res_optimal.append(tmp)
        

    print('res_optimal')
    pprint(res_optimal)    
    return res


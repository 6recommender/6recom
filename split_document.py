## This code written in 2020 by Maha BOUZAIENE <bouzaiene.maha@gmail.com> is licensed under CC BY-NC-SA
## This license allows reusers to distribute, remix, adapt, and build upon the material in any medium
## or format for noncommercial purposes only, and only so long as attribution is given to the creator.
## If you remix, adapt, or build upon the material, you must license the modified material under
## identical terms. 
## CC BY-NC-SA includes the following elements:
## BY – Credit must be given to the creator
## NC – Only noncommercial uses of the work are permitted
## SA – Adaptations must be shared under the same terms
## For further information see https://creativecommons.org/about/cclicenses/
## or look into the directory: ./copyright_NOTICES

import re
import os
import ast
import shutil
import ntpath
from pathlib import Path
from configparser import ConfigParser
conf_parser = ConfigParser( allow_no_value = True )
conf_file = Path( __file__ ).parent / "config.ini"
conf_parser.read( conf_file )

class Extraction:
   def __init__(self,path_file,body_dir,abstract_dir):
       self.path_file = path_file
       self.body_dir = body_dir
       self.abstract_dir = abstract_dir
       
   def abstractextraction(self):
      abstract_max_size = int( conf_parser.get( 'configuration', 'max_size_abstract' ) )
      x = False
      
      abstract_file = shutil.copy( self.path_file, self.abstract_dir )
      
      f = open( abstract_file,"w+",encoding ="utf-8")
      
      ff = open( self.path_file,"r",encoding ="utf-8")
      d = ff.readlines()
      ff.close()

      abstr_sz = 0 
      for l in d:
         l = l.replace("\x0c","")
         i = re.sub('\ |\d|\.|\!|\/|\;|\_|-|\#|\&|\"|\'|\(|\[|\`|\´|\^|\@|\]|\)|\°|\}|\{|\~|\=|\+|\¨|\£|\$|\¤|\*|\µ|\%|\?|\,|\:|\§|\|',"",l)
         i = i.replace("\\","")
         i = i.upper()
         j = l.replace(" ","")
         j = j.upper()
         #print( l )
         if ((i.startswith("INTRODUCTION")) or (i.startswith("BACKGROUND")) or (i=="MOTIVATION") or (i.startswith("ENTRGDUCTION"))
             or (i.startswith("INTRDDUCTION")) or (i.startswith("PREREQUISITES")) or (i=="OVERVIEW") or (i.startswith("CONTEXTANDOBJECTIVES"))
             or (i.startswith("THEREPRESENTATION")) or (i.startswith("TECHNICALAIDS")) or (i.startswith("THECHALLENGE")) or (i.startswith("PURPOSEOFTHEPRESENTSTUDY"))
             or (i.startswith("IDECIDINGHOWTOREPRESENTKNOWLEDGE")) or ((j.startswith("1.")) and (j[2].isalpha()) and (j[3]!="."))
             or ((j.startswith("A.")) and (j[2].isalpha()) and (j[3]!=".")) or ((j.startswith("I.")) and (j[2].isalpha()) and (j[3]!="."))):
            break

         if ((len(l)!=0) and (l[0]!=" ") and (l[0]!="") and (l!="\n") and (x == True)):
            f.write(l)
            abstr_sz += len( l )
            if abstr_sz > abstract_max_size:
               break

         if ((i.startswith("ABSTRACT")) or (i.startswith("RESUME")) or (i.startswith("EXTENDEDABSTRACT")) or (i.startswith("IABSTRACT"))
             or (i.startswith("HIGHLIGHTS")) or (i.startswith("GRAPHICALABSTRACT")) or (i.startswith("KEYWORD")) or (i.startswith("ARTICLEINFO"))or (i.startswith("RÉSUMÉ"))):
            x = True
      print( 'abstract size is {0}'.format( abstr_sz ))    
      f.truncate()
      f.close()
      return x


   def bodyextraction(self):  # not used for now, we work either with the abstract only or the whole document
      x = False
      
      document_file = shutil.copy(self.path_file, self.body_dir)

      f = open( document_file,"w+", encoding ="utf-8" )
      
      ff = open( self.path_file,"r", encoding ="utf-8" )
      d = ff.readlines()
      ff.close()
      
      for l in d:
           l = l.replace("\x0c","")
           i = re.sub('\ |\d|\.|\!|\/|\;|\_|-|\#|\&|\"|\'|\(|\[|\`|\´|\^|\@|\]|\)|\°|\}|\{|\~|\=|\+|\¨|\£|\$|\¤|\*|\µ|\%|\?|\,|\:|\§|\|',"",l)
           i = i.replace("\\","")
           i = i.upper()
           j = l.replace(" ","")
           j = j.upper()
   
           if ((len(l)!=0) and (l[0]!=" ") and (l[0]!="") and (l!="\n") and (x == True)):
              f.write(l)
           
           if ((i.startswith("INTRODUCTION")) or (i.startswith("BACKGROUND")) or (i=="MOTIVATION") or (i.startswith("ENTRGDUCTION"))
               or (i.startswith("INTRDDUCTION")) or (i.startswith("PREREQUISITES")) or (i=="OVERVIEW") or (i.startswith("CONTEXTANDOBJECTIVES"))
               or (i.startswith("THEREPRESENTATION")) or (i.startswith("TECHNICALAIDS")) or (i.startswith("THECHALLENGE")) or (i.startswith("PURPOSEOFTHEPRESENTSTUDY"))
               or (i.startswith("IDECIDINGHOWTOREPRESENTKNOWLEDGE")) or ((j.startswith("1.")) and (j[2].isalpha()) and (j[3]!="."))
               or ((j.startswith("A.")) and (j[2].isalpha()) and (j[3]!=".")) or ((j.startswith("I.")) and (j[2].isalpha()) and (j[3]!="."))):

               x = True
               
      f.truncate()
      f.close()



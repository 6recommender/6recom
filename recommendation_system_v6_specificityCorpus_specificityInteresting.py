## This code written in 2020 by Maha BOUZAIENE <bouzaiene.maha@gmail.com> is licensed under CC BY-NC-SA
## This license allows reusers to distribute, remix, adapt, and build upon the material in any medium
## or format for noncommercial purposes only, and only so long as attribution is given to the creator.
## If you remix, adapt, or build upon the material, you must license the modified material under
## identical terms. 
## CC BY-NC-SA includes the following elements:
## BY – Credit must be given to the creator
## NC – Only noncommercial uses of the work are permitted
## SA – Adaptations must be shared under the same terms
## For further information see https://creativecommons.org/about/cclicenses/
## or look into the directory: ./copyright_NOTICES

import csv
import numpy as np
import pandas as pd
import math
import os
import math
import pickle
from nltk.corpus import wordnet
from stanza_to_list_of_indexed_objects import Stanza_Output
from bert_to_list_of_indexed_objects import BERT_Output
from dynprogalign import dynprogalign, alignment_info
from merge_annotations import *
from text_span_v2 import *
from unification import *
from unification.match import *
from wn_specificity import specificity
from sklearn.metrics.pairwise import cosine_similarity
from collections import Counter
from pathlib import Path
from configparser import ConfigParser
directoryFichierConfig = Path( __file__ ).parent / "config.ini"
conf_parser = ConfigParser( allow_no_value = True )
conf_parser.read( directoryFichierConfig )

def cosine_similarity_two_triplets(triplet1, triplet2):
    sum_liste1 = [0 for i in range(int(conf_parser.get('configuration','max_seq_length')))]

    for t,u in triplet1.items():
        if type(u) is mwu:
            if ( 'BERT_Representation' in u.annotations.keys() ):
                sum_liste1 = [sum(x) for x in zip(sum_liste1, u.annotations['BERT_Representation'])]
    for sl in range(len(sum_liste1)):
        sum_liste1[sl] = sum_liste1[sl]/len(sum_liste1)
#------------------------------------------------------------------------------------------

    sum_liste2 = [0 for i in range(int(conf_parser.get('configuration','max_seq_length')))]

    for t,u in triplet2.items():
        if type(u) is mwu:
            if ( 'BERT_Representation' in u.annotations.keys() ):
                sum_liste2 = [sum(x) for x in zip(sum_liste2, u.annotations['BERT_Representation'])]
    for sl in range(len(sum_liste2)):
        sum_liste2[sl] = sum_liste2[sl]/len(sum_liste2)
        
#------------------------------------------------------------------------------------------                

    x = np.array(sum_liste1).reshape(1, -1)
    y = np.array(sum_liste2).reshape(1, -1)
    cos_similarity = cosine_similarity(x,y)[0][0]

    return cos_similarity


def write_sentence(triplet):
    tmp = {}
    for e in triplet.keys():
        if type(triplet[e]) is mwu:
            tmp.update({e:triplet[e]})
        

    dictionnaire = dict()
    for k in tmp.keys():
        index = tmp[k].annotations['id']
        dictionnaire[index] = tmp[k]
    sentence = ''
    for i in sorted (dictionnaire.keys()) :
        sentence+=dictionnaire[i].annotations['text']+' '
    sentence = sentence.strip()
    
    return (sentence,dictionnaire)


def get_source_text(triplet, document_dir, doc_name, txt_sufx):
    if(Path(document_dir+doc_name+txt_sufx).is_file()):

        with open(document_dir+doc_name+txt_sufx,'r',encoding='utf-8') as f:
            doc = f.read()

        sent, dictionnaire = write_sentence(triplet)

        list_keys = sorted( dictionnaire.keys() )

        start_idx = dictionnaire[ list_keys[0] ].annotations['start_char']

        end_idx = dictionnaire[ list_keys[ len( list_keys )-1 ] ].annotations['end_char']

        sentence = doc[start_idx:end_idx]
        sentence = sentence.replace('\n',' ')
        return sentence


def discriminating_triplet(triplet,dict_uninteresting_articles,threshold):
    for article in dict_uninteresting_articles.keys():
        for tr in dict_uninteresting_articles[article]:
            cos_similarity = cosine_similarity_two_triplets(tr,triplet)
    
            if ( cos_similarity >= threshold ):
                return False
    return True

def get_metadata_nlp4nlp(metadata_file,sep):
    # utiliser separateur ; dans ligne 129
    df_metadata = pd.read_csv(metadata_file,sep=sep, encoding='utf-8-sig')
    dict_metadata = dict()
    for l in np.array(df_metadata):
        line = l[0].split(',')
        file_name = line[0]
        dict_metadata[file_name] = line[1:len(line)]# publication_date,authors,title
    return dict_metadata


def get_metadata_hal(metadata_file,sep):
    # utiliser separateur , dans ligne 130
    df_metadata = pd.read_csv(metadata_file,sep=sep, encoding='utf-8-sig')
    dict_metadata = dict()
    for line in np.array(df_metadata):
        file_name = line[0]
        dict_metadata[file_name] = line[2:5]# publication_date,authors,title
    return dict_metadata


def article_recommendation():
    have_metadata_file = conf_parser.getboolean( 'configuration', 'have_metadata_file' )
    
    if have_metadata_file :     
    #    dict_metadata = get_metadata_nlp4nlp(conf_parser.get('configuration','metadata_file'),';')
        dict_metadata = get_metadata_hal(conf_parser.get('configuration','metadata_file'),',')
    else :
        dict_metadata = None


    file_hal = conf_parser.get('configuration','triplets')
    with open(file_hal,'rb') as f:
        triplets_by_article_corpus_hal = pickle.load(f)

    file_expert = conf_parser.get('configuration','triplets_expert')
    with open(file_expert,'rb') as f:
        triplets_by_article_positif = pickle.load(f)
 
    uninteresting_triplets = conf_parser.get('configuration','uninteresting_triplets')
    with open(uninteresting_triplets,'rb') as f:
        dict_uninteresting_articles = pickle.load(f)


#*****************************************************************************
    for art in triplets_by_article_corpus_hal.keys():
        triplets_article = triplets_by_article_corpus_hal[art]
        triplet_to_delete = []
        for y in triplets_article:
            words = []
            for k in y.keys():
                if type(y[k]) is mwu:
                    words.append(y[k].annotations['text'])

            if (specificity( words ) is not None):
                if ( specificity( words ) < int(conf_parser.get('configuration','specificity_threshold')) ):
                    triplet_to_delete.append(y)
        for d in triplet_to_delete:
            triplets_article.remove(d)



    triplets_domaine = dict()
    print(triplets_by_article_positif.keys())
    for article in triplets_by_article_positif.keys():
        for triplet in triplets_by_article_positif[article]:
            if discriminating_triplet(triplet,dict_uninteresting_articles,float(conf_parser.get('configuration','discrimination_threshold'))):
                if article not in triplets_domaine.keys():
                    triplets_domaine[article] = []
                triplets_domaine[article].append(triplet)
                
    print(len(list(triplets_domaine.keys())))
    print(triplets_domaine.keys())
    list_interesting_triplets_domain = []
    for k in triplets_domaine.keys():
        for t in triplets_domaine[k]:
            list_interesting_triplets_domain.append(t)
        
    
    triplet_to_delete = []
    specificity_dict = dict()
    references_dict = dict()
    print('avant {0}'.format(len(list_interesting_triplets_domain)))
    for y in range(len(list_interesting_triplets_domain)):
        words = []
        for k in list_interesting_triplets_domain[y].keys():
            if type(list_interesting_triplets_domain[y][k]) is mwu:
                words.append(list_interesting_triplets_domain[y][k].annotations['text'])
        print(words)
        if (specificity( words ) is not None):
            if ( specificity( words ) < int(conf_parser.get('configuration','specificity_threshold')) ):
                triplet_to_delete.append(list_interesting_triplets_domain[y])
            else:
                key_name = ''.join(words)
                key_name+=str(y)
                specificity_dict[key_name] = specificity( words )
                references_dict[key_name] = list_interesting_triplets_domain[y]
    for d in triplet_to_delete:
        list_interesting_triplets_domain.remove(d)

    
    print('apres {0}'.format(len(list_interesting_triplets_domain)))


    print(len(list_interesting_triplets_domain))
    print()
    print('====================================================')
    print('Micro-thèmes du domaine:')
    for y in list_interesting_triplets_domain:
        tmp = {}
        for k in y.keys():
            if type(y[k]) is mwu:
                tmp.update({k:y[k].annotations['text']})
        for k,v in references_dict.items():
            if v == y:
                print('{0}-->{1}'.format(tmp,specificity_dict[k]))

    print('====================================================')
    articles_to_recommend = dict()
    compteur = 0
    for article in triplets_by_article_corpus_hal.keys():
        for triplet in triplets_by_article_corpus_hal[article]:
            for topic in list_interesting_triplets_domain:
                cos_similarity = cosine_similarity_two_triplets(topic,triplet)
                if ( cos_similarity >= float(conf_parser.get('configuration','filtering_threshold')) ):
                    if article not in articles_to_recommend.keys():
                        articles_to_recommend[article] = []
                    if triplet not in articles_to_recommend[article]:
                        articles_to_recommend[article].append((triplet,cos_similarity))
                        compteur+=1
    #============================Display the results=====================================================

    for article in list(articles_to_recommend.keys()):
        list_sentences = []
        articles_to_recommend[article].sort(key=lambda tup: tup[1],reverse=True)

        for tr in articles_to_recommend[article]:            
            triplet,cos_sim = tr
            sentence = write_sentence(triplet)[0]
            sentence = get_source_text(triplet, conf_parser.get('configuration','abstract_dir'), article, conf_parser.get( 'configuration', 'txt_sufx' ))

            if not any( sentence in x for x in list_sentences ):
                list_sentences.append((sentence,cos_sim))
 
        print('Article {0} is recommended '.format(article))
        if dict_metadata != None:
    #--------------"metadata_by_article_corpus_hal.csv / metadata_dnn.csv / metadata_hydrate_slurry.csv"-----------------------------------
            print('           Title: {0}'.format(dict_metadata[article][2]))
            print('           Authors: {0}'.format(dict_metadata[article][1]))
            print('           Published in: {0}'.format(dict_metadata[article][0]))

    #--------------"globaleACL.csv"-------------------------------------------------------
    ##        print('           Title: {0}'.format(dict_metadata[article][2]))
    ##        print('           Published in: {0}'.format(dict_metadata[article][1]))


        for se in list_sentences:
            print('           {0}'.format(se))
        print()
    print(len(list(articles_to_recommend.keys())))
    print(compteur)

article_recommendation()

# This code written by Patrick Paroubek <pap@limsi.fr> is licensed under CC BY-NC-SA
# This license allows reusers to distribute, remix, adapt, and build upon the material in any medium
# or format for noncommercial purposes only, and only so long as attribution is given to the creator.
# If you remix, adapt, or build upon the material, you must license the modified material under
# identical terms. 
# CC BY-NC-SA includes the following elements:
# BY – Credit must be given to the creator
# NC – Only noncommercial uses of the work are permitted
# SA – Adaptations must be shared under the same terms
# For further information see https://creativecommons.org/about/cclicenses/
# or look into the directory: ./copyright_NOTICES

import sys
import nltk
from nltk.corpus import wordnet as wn

from pprint import pprint

from nltk.corpus import wordnet as wn

from collections import OrderedDict

def compute_depth( hypers ):
    assert( type( hypers ) is list )
    assert( False not in [ type( x ) is list for x in hypers ] )
    for x in hypers:
        for s in x:
            assert( type( x ) is nltk.corpus.wordnet.Synset )
    #------------
            

def depth_to_root( hypers ):
    assert( type( hypers ) is list )
    assert( len( hypers ) > 0 )
    assert( False not in [ type( x ) is list for x in hypers ] )
    for x in hypers:
        for s in x:
            assert( type( s ) is nltk.corpus.reader.wordnet.Synset ) 
    #------------
    if len( hypers) == 0:
        return 0
    else:
        newhyplst = []
        for hyplst in hypers:
                h = hyplst[ -1 ]
                new_hypers = h.hypernyms( )
                for newh in new_hypers:
                    newhyplst.append( hyplst + [ newh ] )
        if len( newhyplst ) > 0:
            return depth_to_root( newhyplst )
        else:
            minlen = min( list( map( len, hypers))); minpath2root = []
            for hlst in hypers:
                if len( hlst ) == min( list( map( len, hypers ))):
                          minpath2root = hlst
            return (minlen, minpath2root)

def specificity( words ):
    assert( type( words ) is list )
    assert( False not in [ type( x ) is str for x in words ] )
    all_syns = {}
    for w in words:
        syns = wn.synsets( w )
        assert( type( syns ) is list )
        if len( syns ) > 0 :
            aux = []
            for s in syns:
                assert( type( s ) is nltk.corpus.reader.wordnet.Synset)
                aux.append( [ s ] )
            all_syns[ w ] = aux
        else:
            all_syns[ w ] = []
    all_depths = {}
    all_paths = {}
    specificity = 0
    not_in_wn = 0
    for w in words:
        if len( all_syns[ w ]) > 0:
            depth, path = depth_to_root( all_syns[ w ] )
            all_depths[ w ] = depth
            specificity += depth
            all_paths[ w ] = path
        else:
            not_in_wn += 1
            all_depths[ w ] = 0
            all_paths[ w ] = []
##    for w in words:
##        print( '{0} has wn specificity {1} \n\t and min hypernym path {2}'.format( w, all_depths[ w ], all_paths[ w ] ))
    if specificity == 0:
        return None
    else:
        return specificity + ((specificity / (len( words) - not_in_wn)) * not_in_wn)
        

##print( '# Bérengère:' )
##for words in [ 'most convection model provide dependence prediction', 'natural convection transport heat', 'corrugate pipe develop pipe flow', 'corrugate pipe analyze develope pipe flow',  ]:
###for words in ['we make that','neural network models for natural language processing tasks have been increasingly focused on for their ability']:
##    words = words.strip().split()
##    print( '\"{0}\" has wn specificity {1}'.format( words, specificity( words )))
##    print('===========================================================' )
##print( '# Michel:' )
##for words in [ 'CO2 hydrate  have minor composition effect', 'carbon dioxyde hydrate have minor composition effect', 'model consider size expression',  'hydrate slurry fulfil other condition criterion', 'literature study assemble measurement', 'heat ﬂuid have good propertiy']:
##    words = words.strip().split()
##    print( '\"{0}\" has wn specificity {1}'.format( words, specificity( words )))
##    print('===========================================================' )







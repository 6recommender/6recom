## MIT License
##
## Copyright (c) 2019 aakorolyova
##
## Permission is hereby granted, free of charge, to any person obtaining a copy
## of this software and associated documentation files (the "Software"), to deal
## in the Software without restriction, including without limitation the rights
## to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
## copies of the Software, and to permit persons to whom the Software is
## furnished to do so, subject to the following conditions:
##
## The above copyright notice and this permission notice shall be included in all
## opies or substantial portions of the Software.
##
## HE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
## IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
## FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
## AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
## LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
## OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
## SOFTWARE.
## Note: A large part of this file comes initially from (2019):
## https://github.com/aakorolyova/DeSpin/blob/master/text_span.py
## with additions in 2020 by Maha BOUZAIENE <bouzaiene.maha@gmail.com>
## and Patrick PAROUBEK <pap@limsi.fr>

import sys
import glob
import codecs
import os
import collections
import io
from copy import deepcopy

from functools import partial
from collections import defaultdict
from unification import *
from unification.match import *

from pprint import pprint

##DEBUG = True
DEBUG = False


@unifiable    
class mwu( object ):
    #--- class variables 
    default_mwu_name_prefix = 'MWU'
    default_mwu_cnt = 0
    # txsps is a list of pairs of character offsets e.g.  mwu( nm='MWU_4', txtsps=[ (13300 , 13320)])
    # example of mwu structure:
    # m1=mwu( nm='MWU_4', txtsps=[ (13300 , 13320)], annotations={'comments':'ceci est un test', 'POS_GRACE':'Ncms'})
    
    def __init__( self, nm = None, txtsps = None, typ = None, annotations = None ):
## # commented out for use with unify module        
##        assert( type( txtsps ) is list )
        #--- name.
        if( nm is None ):
            self.name = mwu.default_mwu_name_prefix + '_' + str( mwu.default_mwu_cnt ); mwu.default_mwu_cnt += 1
        else:
            self.name = nm
        if( txtsps is None ):
             self.txtspans = []
        else:
            self.txtspans = txtsps
        #--- annotations global to the mwu (attribute value associations in a dictionary).
        if( annotations is None ):
            self.annotations = {}
        else:
            self.annotations = annotations
        if( typ is None ):
             self.typ = ''
        else:
            self.typ = typ

    def text_n( self, doc, n ):
        tsp = self.txtspans[ n ]
        assert( type( tsp ) is tuple )
        return( doc.span( tsp[0], tsp[1] ))
                
    def text( self , doc, sep = None ):
        res = ''
        for i in range( 0, len( self.txtspans ) ):
                if( sep is None ):
                    res += self.text_n( doc, i )
                else:
                    if( i > 0 ):
                        res += sep
                    res += self.text_n( doc, i )  
        return( res )

    def span_n( self, n ):
        if( n < len( self.txtspans) ):
            return( self.txtspans[ n ] )
        else:
            return( None )

    def __repr__( self ):
        res = 'mwu( nm=' + self.name.__repr__() +  ', txtsps=['
        n = 0
        for sp in self.txtspans:
            res += sp.__repr__()
            ##            res += 'wnts( \'SPAN\', ' +  str( sp[0] ) + ',' + str( sp[1] ) + ')'
            n += 1
            if( n < len( self.txtspans )):
                res += ', '
        res +=  ']'
        res += ', typ= {0}'.format( self.typ.__repr__() )
        if self.annotations:
            res += ', annotations={0}'.format( self.annotations.__repr__() )
        res += ')'
        return( res ) 

    def __str__( self ):
        res = '---mwu:\n\tname= ' + str( self.name )
        res += '\n\ttextspans is: ' + str( self.txtspans )
        res += '\n\ttyp is: ' + str( self.typ )
        res += '\n\tannotations is: ' + str( self.annotations )
        res += '\n'
        return( res )

    def size( self ):
        return( len( self.txtspans ) )
    
         #---- annotations of the whole mwu)
    def set_annot( self, aspect, value ):
        self.annotations[ aspect ] = value

    def get_annot( self, aspect ):
        return( self.annotations[ aspect ] )

    def append( self, m ):
        # append all the text spans of m to the list of self.txtspans
        # and incorporate annotations of m onto self.annotations
        if len( m.txtspans ) > 0:
            for s in m.txtspans:
                self.txtspans.append( deepcopy( s ))
            self.annotations.update( m.annotations )
        return self
        
#--- end of mwu class

@unifiable
class rel( object ):
    def __init__( self, nm = None, src = None, trg = None, typ = None ):
        assert( type( nm is str) or type( nm is Var))
        assert( type( src is mwu) or type( src is Var) or (type( src ) is relation))
        assert( type( trg is mwu) or type( trg is Var) or (type( trg ) is relation))
        assert( type( typ is str) or type( typ is Var))
        self.name = nm 
        self.src = src 
        self.trgt = trg 
        self.typ = typ

    def __repr__( self ):
        res = 'rel( nm=' + self.name.__repr__() + ', src=' +  self.src.__repr__() + ', trg=' + self.trgt.__repr__() + ', typ=' + self.typ.__repr__() + ')'
        return( res )

    def __str__( self ):
        sout = 'rel: nnm= ' + str(self.name) + ' typ= ' +  str(self.typ) + ' src= ' +  str(self.src)  + ' trg= ' + str(self.trgt) + '\n'
        return( sout )

    
@unifiable
class relation( rel ):
    # NOTE: not yet used
    def __init__( self, nm=None, src=None, trg=None, annotations = None, typ = None ):
         assert( (type( nm) is str) or (type( nm ) is Var))
         assert( (type( src ) is mwu) or (type( src ) is Var) or (type( src ) is relation) )
         assert( (type( trg ) is mwu)  or (type( trg ) is Var) or (type( trg ) is relation) )
         assert( (type( typ ) is str) or (type( typ ) is Var))
         if( annotations is None ):
             self.annotations = {}
         else:
             self.annotations = annotations
                 
         rel.__init__( self, nm,  src, trg, typ )
         
     #---- annotations of the whole mwu)
    def set_annot( self, aspect, value ):
        self.annotations[ aspect ] = value

    def get_annot( self, aspect ):
        return( self.annotations[ aspect ] )

    def __repr__( self ):
        res = 'rel( nm=' + self.name.__repr__() + ',src=' +  self.src.__repr__() + ', trg=' + self.trgt.__repr__() + ', typ=' + self.typ.__repr__()
        if self.annotations:
            res += 'annotations=' + self.annotations.__repr__()
        res += ')'
        return( res )

    def __str__( self ):
        sout = 'rel: nnm= ' + str(self.name) + ' typ= ' +  str(self.typ) + ' src= ' +  str(self.src)  + ' trg= ' + str(self.trgt) + '\n'
        return( sout )
    

#--- end of class relation    
@unifiable
class construction( object ):
    #--- class variables   
    # given to namespace(),  here is the list of various type names used to build object names (unique identifiers)
    SPAN_TYP = 'SPAN'
    MWU_TYP = 'MWU'
    SRC_TYP = 'SRC'
    TRGT_TYP = 'TRGT'
    REL_TYP = 'REL'
    CONSTRU_TYP = 'CONSTRU'
    ALL_TYPS =  [ SPAN_TYP, MWU_TYP, SRC_TYP, TRGT_TYP, REL_TYP, CONSTRU_TYP  ]
    #------

    def __init__( self, nm=None, mwu_lst = None, rel_lst=None ):
        
        assert( (mwu_lst is None) or (type( mwu_lst ) is list) or (type( mwu_lst) is dict ))
        if( nm is None ):
            self.name = ''
        else:
            assert( (type( nm ) is str) or (type( nm ) is Var) )
            self.name = nm
            
        if( mwu_lst is None ):
            self.mwus = {}
        else:
            if type( mwu_lst ) is list:
                assert( False not in [((type( m ) is mwu) or (type( m ) is Var)) for m in mwu_lst ])
                for e in mwu_lst:
                    self.mwus[ e.name ] = e
            else:
                assert( type( mwu_lst ) is dict) 
                self.mwus = mwu_lst
                
        if( rel_lst is None ):
            self.rels = {}
        else:
            if type( rel__lst ) is list:
                assert( False not in [((type( m ) is rel) or (type( m ) is Var)) for m in rel_lst ])
                for e in rel_lst:
                    self.rels[ e.name ] = e
            else:
                assert( type( rel_lst ) is dict) 
                self.rels = rel_lst

    def __repr__( self ):
        sout = 'construction( nm=' +  self.name.__repr__() + ', rels=['
        n = 0
        for r in self.rels:
            sout += r.__repr__()
            n += 1
            if( n < len( self.rels ) ):
                sout += ', '
        sout += ']'
        return( sout )  

    def __str__( self ):
        sout = 'construction: name= ' + str(self.name) + 'rels='
        for r in self.rels:
            sout += r.__str__()
        sout += '\n'
        return( sout )
    #--- mwus

    def add_mwu( self, m ):
        assert( (type( m ) is mwu) or (type(m) is list) or (type(m) is dict) )
        if( type( m ) is list ):
            assert( False not in [((type( x ) is mwu) or (type( x ) is Var)) for x in m ])
            for e in m:
                self.mwus[ e.name ] = e
        elif type( m ) is dict:
            assert( False not in [((type( x ) is mwu) or (type( x ) is Var)) for x in m.values() ])
            self.mwus.update( m )
        else:
            assert( type( m ) is mwu )
            self.mwus[ m.name ] = m
            
    def remove_mwu( self, m ):
        assert( (type( m ) is mwu) or (type(m) is list) or (type(m) is dict) )
        if( type( m ) is list ):
            for e in m:
                if e.name in self.mwus.keys():
                    del self.mwus[ e.name ]
        else:
            if m.name in self.mwus.keys():
                del self.mwus[ m.name ]

    def get_mwu( self, k = None ):
        assert( k is not None)
        if k in self.mwus.keys():
            return self.mwus[ k ]
        else:
            return None

    def find_mwu_containing_span( self, s ):
        assert( (type( s ) is tuple) and (len(s) == 2) and (type(s[0]) is int) and (type(s[1]) is int) and (s[0] < s[1]) )
        for (nm, mw) in self.mwus.items():
                if( mw.contains_span_p( s ) ):
                    return( mw )
        return( None )

    def find_mwu_with_name( self, name ):
        return get_mwu[ name ]

    def get_mwus_with_type( self, typ ):
        res = []
        for m in self.mwus:
            if m.typ == typ:
                res.append( m )
        return res

    def get_mwu_text( self, m ):
        assert( type( m ) is mwu )
        res = ''
        for (first, last) in m.txtspans:
            if res != '':
                res += ' '
            res += self.ctnt[ first:last ]
        return res
            
    #--- relations 

    def add_rel( self, r ):
        assert( (type( r ) is relation) or (type(r) is list) or (type( r) is dict ))
        if( type( r ) is list ):
            for e in r:
                self.rels[ e.name ] = e
        elif type( r ) is dict:
            self.rels.update( r )
        else:
            assert( type( r ) is relation )
            self.rels[ r.name ] = r

    def remove_rel( self, r ):
        assert( (type( r ) is relation) or (type(r) is list) or (type( r ) is dict) )
        if( type( r is list ) ):
            for e in r:
                if e.name in self.rels.keys():
                    del self.rels[ e.name ]
        elif type( r ) is dict:
            for (nm, rel) in r:
                if nm in self.rels.keys():
                    del self.rels[ nm ]
        else:
            assrt( type( r ) is relation )
            del self.rels[ r.name ]


match = partial(match, Dispatcher=VarDispatcher)
env = Var( 'env' )
A = Var( 'A' )
B = Var( 'B' )

@match( A, B, env )
def unifylist( A, B, env = {} ):
    #print( 'A=={0} B=={1} env=={2} len(A)=={3} len(B)=={4}'.format( A, B, env, len(A), len(B) ) )
    local_env0 = deepcopy( env )
##    print( type( A ) )
##    print( type( B ) )
    if type( A ) is tuple:
##        print( 'debug0' )
        if len( A ) > 0:
##            print( 'debug1' )
            if type( B ) is tuple:
##                 print( 'debug2' )
                 if len( A ) == 1:
                     match_res = unify( A[ 0 ], B, dict(local_env0) )
                     if match_res:
                         local_env3 = deepcopy( dict( local_env0 ) )
                         local_env3.update( match_res )
                         return local_env3
                     else:
                         return False
                 else:
                     i = 1
                     j = 1
                     while i <= len( A ):
##                        print( 'debug3' )
##                        print( '\ti=={0}'.format( i ))
                        local_env1 = deepcopy( local_env0 )
                        while j < len( B ):
##                            print( 'debug4' )
##                            print( '\t\tj=={0}'.format( j ))
                            match_res = unify( A[ 0:i ], B[ 0:j ], dict(local_env1) )
                            if match_res:
##                                print( 'debug5' )
                                local_env2 = deepcopy( dict(local_env1) )
                                local_env2.update( match_res )
                                match_res2 = unifylist( A[ i: ], B[ j: ], local_env2 )
                                if match_res2:
##                                    print( 'debug6' )
                                    local_env2.update( dict(match_res2) )
                                    return local_env2
                                else:
                                    return False
                            else:
                                j += 1
                                local_env2 = local_env1
                        if type( A[ 0:i][-1] ) is Var:
##                            print( 'debug7' )
                            i += 1
                            j = 1
                            local_env1 = local_env0
                        else:
                            return unify(  A, B, dict(local_env0) )
            else:
##                  print( 'debug8' )
                  match_res = unify(  A, B, dict(local_env0) )
                  if match_res:
##                    print( 'debug9' )
                    local_env0.update( match_res )
                    return local_env0
                  else:
                      return False
        else:
##            print( 'debug10' )
            assert( len( A ) == 0 )
            match_res = unify( [], B, dict(local_env0) )
            if match_res:
##                print( 'debug11' )
                local_env.update( match_res )
                return local_env0
            else:
                return False
    else:
##        print( 'debug12' )
        match_res = unify( A, B, dict(local_env0) )
        if match_res:
##            print( 'debug13' )
            local_env.update( match_res )
            return local_env0
        else:
            return False

   
@unifiable
class construction_pattern( construction ):
    def __init__( self, nm= '', mwu_lst = {}, rel_lst = {}, mwu_lst_opt = {}, rel_lst_opt = {}):
        assert( (type(mwu_lst) is dict) and (type(rel_lst) is dict) )
        assert( (type(mwu_lst_opt) is dict) and (type(rel_lst_opt) is dict) )
        assert( False not in [ ( type( m ) is mwu ) for m in list(mwu_lst.values()) ] )
        assert( False not in [ ( type( r ) is relation ) for r in list(rel_lst.values()) ] )
        assert( False not in [ ( type( m ) is mwu ) for m in list(mwu_lst_opt.values()) ] )
        assert( False not in [ ( type( r ) is relation ) for r in list(rel_lst_opt.values()) ] )
    
        self.name = nm
        self.mwus = mwu_lst
        self.rels = rel_lst
        self.mwus_opt = mwu_lst_opt
        self.rels_opt = rel_lst_opt

        self.env = {}

       # match tous les élements de ma construction (construction_pattern) avec tous les élements de kstruct donnée        
    def match( self, kstruct, env = {} ):
        assert( type( kstruct ) is construction )

        self.env = env

        for pm in self.mwus.keys():
            
            for m in kstruct.mwus.keys():

                mres = unify( self.mwus[pm],kstruct.mwus[m], self.env)

                if mres:
#                    print('-------MATCH FOUND-----------')
                    self.env.update( mres )
#                    print(self.env)
                else:
                    return False


        for pr in self.rels.keys():
            
            for r in kstruct.rels.keys():
#                print(type(r))
                relres = unify( self.rels[pr], kstruct.rels[r], self.env)
                if relres:
#                    print('-------MATCH FOUND-----------')
                    self.env.update( relres )
#                    print(self.env)
                else:
                    return False#break


#====================Optional=======================================

        for pm in self.mwus_opt.keys():
            for m in kstruct.mwus.keys():
                mres = unify( self.mwus_opt[pm], kstruct.mwus[m], self.env)
                if mres:
                    self.env.update( mres )

                
        for pr in self.rels_opt.keys():
            for r in kstruct.rels.keys():
                relres = unify( self.rels_opt[pr], kstruct.rels[r], self.env)
                if relres:
                    self.env.update( relres )
                

        return self.env

    def match2( self, kstruct, env = {} ):
        assert( type( kstruct ) is construction )
        
        self.env = env
        mres = unifylist( self.mwu_pattern_var_lst, kstruct.mwus, self.env )
        if mres:
            self.env.update( mres )
        else:
            pass#return False
#        print(self.rel_pattern_var_lst)
#        print(type(kstruct.rels))
#        print(type(self.rels))
        relres = unifylist( self.rels, kstruct.rels, self.env )
        if relres:
#            print('---------HELLO----------------------')
            self.env.update( relres )
        else:
            return False
        
        return self.env

    # match partiel (au moins un match) des élements de ma construction (construction_pattern) avec tous les élements de kstruct donnée
    def search( self, kstruct, env = {} ):
        self.env = env
        for pm in self.mwus.keys():
            match_found = False
            for m in kstruct.mwus.keys():
                mres = unify( self.mwus[pm], kstruct.mwus[m], self.env)
                if mres:
                    self.env.update( mres )
                    match_found = True
                    break
            if not match_found:
                return False
        for pr in self.rels.keys():
            match_found = False
            for r in kstruct.rels.keys():
                relres = unify( self.rels[pr], kstruct.rels[r], self.env)
                if relres:
                    self.env.update( relres )
                    match_found = True
                    break
            if not match_found:
                return False
#====================Optional=======================================

        for pm in self.mwus_opt.keys():
            for m in kstruct.mwus.keys():
                mres = unify( self.mwus_opt[pm], kstruct.mwus[m], self.env)
                if mres:
                    self.env.update( mres )


        for pr in self.rels_opt.keys():
            for r in kstruct.rels.keys():
                relres = unify( self.rels_opt[pr], kstruct.rels[r], self.env)
                if relres:
                    self.env.update( relres )


        return self.env
    
    def search_relation_list(self,rel_List, env = {} ):
        assert( (type(rel_List) is list ) and (not [ x for x in rel_List if not type( x ) is relation ]) )
        copy_rel_List = deepcopy(rel_List)
        self.search_relation_list_data = []
        self.search_relation_list_data = copy_rel_List
        self.env = env

        rel = None
        rel_to_remove =[]
        for pr in self.rels.keys():
            found = False

            for r in self.search_relation_list_data:
                res = unify(self.rels[pr],r,self.env)
                if res:
                    self.env.update(res)
                    found = True
                    rel_to_remove.append(r)
                    #rel_List.remove(r)
                    break
        
            if not found:
                for r in rel_to_remove:
                    if r in self.search_relation_list_data:
                        self.search_relation_list_data.remove(r)

                if ( len(rel_to_remove)!=0 ) :
                    return True

                else:
                    return False

        
        local_env = deepcopy(self.env)
        local_env_rs = deepcopy(self.env)
        #print('local_env_rs')
        #print(local_env_rs.keys())
#===============Optional======================
        for pr in self.rels_opt.keys():
            for r in self.search_relation_list_data:
                local_env = dict()
                local_env.update(local_env_rs)
                res = unify(self.rels_opt[pr],r,local_env)

                if res:
                    
                    new_var_keys = [e for e in res.keys() if e not in local_env_rs.keys()]
                    keys_first_occurence  = [e for e in res.keys() if e not in self.env.keys()]


                    
                    if len(keys_first_occurence)!=0:
                        self.env.update(res)
                        rel_to_remove.append(r)
                    
                    else:

                        for x in new_var_keys:
                            compteur_key = 1
                            str_x = str(x).replace('~','')
                            
                            while( str_x+'_'+str(compteur_key) in list(self.env.keys()) ):
                                compteur_key+=1
                            
                            res[str_x+'_'+str(compteur_key)] = res.pop(x)
                        self.env.update(res)
                        rel_to_remove.append(r)


        for r in rel_to_remove:
            if r in self.search_relation_list_data:
                self.search_relation_list_data.remove(r)
        
#        list_keys = self.env.keys()
#        for k in list_keys:
#            new_key = str(k).replace('~','')
#            self.env[new_key] = self.env.pop(k)


        return self.env


    def search_aux_relation_list(self,env={}):
        self.env = env
        rel = None
        rel_to_remove =[]
        for pr in self.rels.keys():
            found = False
            
            for r in self.search_relation_list_data:
                res = unify(self.rels[pr],r,self.env)
                if res:
                    self.env.update(res)
                    found = True
                    rel_to_remove.append(r)
                    #rel_List.remove(r)
                    break
            
            
            if not found:
                for r in rel_to_remove:
                    if r in self.search_relation_list_data:
                        self.search_relation_list_data.remove(r)
                
                if ( len(rel_to_remove)!=0 ) :
                    return True

                else:
                    return False
        
        
        local_env = deepcopy(self.env)
        local_env_rs = deepcopy(self.env)
        #print('local_env_rs')
        #print(local_env_rs.keys())
#===============Optional======================

        for pr in self.rels_opt.keys():
            for r in self.search_relation_list_data:
                local_env = dict()
                local_env.update(local_env_rs)

                res = unify(self.rels_opt[pr],r,local_env)
                if res:
                    new_var_keys = [e for e in res.keys() if e not in local_env_rs.keys()]
                    keys_first_occurence  = [e for e in res.keys() if e not in self.env.keys()]



                    if len(keys_first_occurence)!=0:
                        self.env.update(res)
                        rel_to_remove.append(r)

                    else:

                        for x in new_var_keys:
                            compteur_key = 1
                            str_x = str(x).replace('~','')

                            while( str_x+'_'+str(compteur_key) in list(self.env.keys()) ):
                                compteur_key+=1

                            res[str_x+'_'+str(compteur_key)] = res.pop(x)
                        self.env.update(res)
                        rel_to_remove.append(r)


        for r in rel_to_remove:
            if r in self.search_relation_list_data:
                self.search_relation_list_data.remove(r)
        
#        list_keys = list(self.env.keys())
#        for k in list_keys:
#            new_key = str(k).replace('~','')
#            self.env[new_key] = self.env.pop(k)


        
        return self.env



    def next_search_relation_list(self,env):
        return self.search_aux_relation_list(env)




        
        
    # match tous les élements de ma construction (construction_pattern) avec tous les élements de liste de kstruct donnée
    def search_list( self, kstruct_lst, env = {} ):
#         pprint( [ type( x ) for x in kstruct_lst ])
         assert( (type( kstruct_lst ) is list) and (not [ x for x in kstruct_lst if not type( x ) is construction ]))
         for c in kstruct_lst:
             kres = self.match( c )
             if not kres :
                 return False
             else:
                 env.update( kres )
                 return env
         

class document( construction ):
    def __init__(self, ident=None, content=None, metadata=None, multi_word_units=None, relations=None, constructions=None ):
        if( (type(ident) is None) or (type(ident) is not str) ):
            raise ValueError
        else:
             self.id = ident

        if( metadata is None ):
            self.metadata = ''
        else:
            if( type(metadata) is not str ):
                raise ValueError
            else:
                 self.metadata = metadata
        if( content is None ):
            self.ctnt = ''
        else:
            if(  type(content) is not str ):
                 raise ValueError
            else:
                 self.ctnt = content
                 
        # potentialy a document has multi-word units (mwus) annotations, relations (between two mwus, a source and a target)
        # and constructions (sets of relations)
        
        if( multi_word_units is None ):
            self.mwus = {}
        else:
            self.mwus = multi_word_units
            
        if( relations is None ):
            self.rels = {}
        else:
            self.rels = relations
            
        if( constructions is None ):
            self.kstructs = {}
        else:
            self.kstructs = constructions

    def len( self ):
        return( len( self.ctnt ) )

    def byte_len( self ):
        return( len( self.ctnt.encode('utf8') ))

    def content( self ):
        return( self.ctnt )

    def span( self, first, last ):
        return( self.ctnt[ first:last ] )

    def __repr__( self ):
        res = 'document( ident=\'' + self.id + '\''
        res += '\t\n, content=' + self.ctnt.__repr__()
        res += '\t\n, metadata=' + self.metadata.__repr__()
        res += '\t\n, multi_word_units = ['
        i=0
        for m_nm in self.mwus.keys():
            if( i > 0 ):
                res += ', '
            i += 1
            res += self.mwus[ m_nm ].__repr__()
        res += ']\n'
        res += '\t, relations= ['
        i = 0
        for r_nm in self.rels.keys():
            if( i > 0 ):
                res += ', '
            i += 1
            res += self.rels[ r_nm ].__repr__()
        res += ']\n'
        res += '\t, constructions= ['
        i = 0
        for k_nm in self.kstructs:
            if( i > 0 ):
                res += ', '
            i += 1
            res += slef.kstructs[ k_nm ].__repr__()
        res += '] )\n'
        return( res )

    def __str__( self ):
        res = 'document is:' + '\tid= ' + str( self.id )
        res += '\tmetadata= ' + str( self.metadata )
        res += '\tlen(self.ctnt)= ' + str( len( self.ctnt ))
        res += '\tlen(mwus)=' + str( len(self.mwus) )
        res += '\tlen(rels)=' + str( len(self.rels) )
        return( res )

    #------- file io

    def read_text( self, path ):
        assert( (type( path ) is str) and (path != '') )
        buffer_sz = 100
        in_strm = codecs.open( path, mode='r', encoding='utf-8', errors='strict', buffering = -1)
        assert( in_strm )
        in_data = u''
        for l in in_strm.readlines( buffer_sz ):
            in_data += l
        self.ctnt = self.ctnt + in_data
        in_strm.close()

    def write_text( self, path ):
        assert( (type( path ) is str) and (path != '') )
        out_strm = codecs.open( path, mode='w', encoding='utf-8', errors='strict', buffering = -1)
        assert( out_strm )
        out_strm.write( self.ctnt )
        out_strm.close()

    
            
#--- end of class document

DEMO_UNIFICATION=False
#DEMO_UNIFICATION=True
if DEMO_UNIFICATION:
    r1 = relation( nm='R1',
                        src=mwu(nm='m1', txtsps=[(0,10)]),
                        trg=mwu( nm='m2',txtsps=[(15,20)]),
                        annotations={ 'PROP1':'valeur_prop'},
                        typ='SUJ-V' )
    r2 = relation( nm='R2',
                        src=mwu(nm='m1', txtsps=[(0,10)]),
                        trg=mwu( nm='m3',txtsps=[(25,30)]),
                        annotations={ 'PROP2':'valeur_prop2'},
                        typ='SUJ-V' )
    r3 = relation( nm='R3',
                        src=mwu(nm='m1', txtsps=[(30,40)]),
                        trg=mwu( nm='m3',txtsps=[(45,50)]),
                        annotations={ 'PROP3':'valeur_prop3'},
                        typ='CPL-V' )

    all_rels = [ r1, r2, r3 ]

    relvar=var('relvar')
    srcvar=var('srcvar')
    trgvar=var('trgvar')
    annotvar=var('annotvar')
    typvar=var('typvar')

    print( [unify( relation( relvar, srcvar, trgvar, annotvar, typvar), r ) for r in all_rels ])
    print( '----------------' )
    print( [unify( relation( relvar, srcvar, trgvar, annotvar, 'SUJ-V' ), r ) for r in all_rels ])
    print( '----------------' )

##>>> 
## RESTART: /media/pap/HDTOSHIBA1T/work/MIROR/TOOLS/python/constructions/cg_ak5/construkt/text_span.py 
##[{~srcvar: 'm1', ~typvar: 'SUJ-V', ~relvar: 'R1', ~trgvar: 'm2', ~annotvar: {'PROP1': 'valeur_prop'}},
## {~srcvar: 'm1', ~typvar: 'SUJ-V', ~relvar: 'R2', ~trgvar: 'm3', ~annotvar: {'PROP2': 'valeur_prop2'}},
## {~srcvar: 'm1', ~typvar: 'CPL-V', ~relvar: 'R3', ~trgvar: 'm3', ~annotvar: {'PROP3': 'valeur_prop3'}}]
##----------------
##[{~srcvar: 'm1', ~relvar: 'R1', ~trgvar: 'm2', ~annotvar: {'PROP1': 'valeur_prop'}},
## {~srcvar: 'm1', ~relvar: 'R2', ~trgvar: 'm3', ~annotvar: {'PROP2': 'valeur_prop2'}},
## False]
##----------------
##>>>



          


